----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/27/2020 12:14:06 PM
-- Design Name: 
-- Module Name: full_adder_4bits_int - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Louka 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity full_adder_4bits_int is
    Port ( A_i : in STD_LOGIC_VECTOR (3 downto 0);
           B_i : in STD_LOGIC_VECTOR (3 downto 0);
           Carry_i : in STD_LOGIC;
           Carry_o : out STD_LOGIC;
           S_o : out STD_LOGIC_VECTOR (3 downto 0));
end full_adder_4bits_int;

architecture Behavioral of full_adder_4bits_int is
    signal sum : unsigned(4 downto 0);
    signal sig_carry : integer range 0 to 1;
begin

    sig_carry <= 1 when Carry_i = '1' else 0;
    sum <= to_unsigned(to_integer(unsigned(A_i))+to_integer(unsigned(B_i)) + sig_carry, sum'length);
    Carry_o <= std_logic(sum(4));
    S_o <= std_logic_vector(sum(3 downto 0));
end Behavioral;
