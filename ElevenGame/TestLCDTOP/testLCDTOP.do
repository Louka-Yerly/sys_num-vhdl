#=========================================
#		Initialisation / Etat Off
#=========================================
restart
force -freeze sim:/testLCDTop/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/testLCDTop/rst 1 0
force -freeze sim:/testLCDTop/Etat_System 000 0
force -freeze sim:/testLCDTop/Etat_Player 0 0
force -freeze sim:/testLCDTop/NextCagnotte_P1 00101000 0
force -freeze sim:/testLCDTop/NextCagnotte_P2 00101000
force -freeze sim:/testLCDTop/NextJackPot 0000000 0
force -freeze sim:/testLCDTop/Dice_1 000 0
force -freeze sim:/testLCDTop/Dice_2 000 0
force -freeze sim:/testLCDTop/Dice_Effect 00000000 0
run 100 ns

force -freeze sim:/testLCDTop/rst 0 0
run 31250 ns

#=========================================
#		Test Top: Etat Player1
#=========================================
force -freeze sim:/testLCDTop/Etat_System 001 0
run 406250 ns


#=========================================
#		Test Top: Etat Waiting
#=========================================
#Test simple
force -freeze sim:/testLCDTop/Etat_System 010 0
force -freeze sim:/testLCDTop/NextCagnotte_P1 00100001 0
force -freeze sim:/testLCDTop/NextCagnotte_P2 00101000 0
force -freeze sim:/testLCDTop/NextJackPot 0000111 0
force -freeze sim:/testLCDTop/Dice_1 011 0
force -freeze sim:/testLCDTop/Dice_2 001 0
force -freeze sim:/testLCDTop/Dice_Effect 11111001 0
run 406250 ns

#Test -15
force -freeze sim:/testLCDTop/NextCagnotte_P1 00011001 0
force -freeze sim:/testLCDTop/NextCagnotte_P2 00101000 0
force -freeze sim:/testLCDTop/NextJackPot 0001111 0
force -freeze sim:/testLCDTop/Dice_1 000 0
force -freeze sim:/testLCDTop/Dice_2 000 0
force -freeze sim:/testLCDTop/Dice_Effect 11110001 0
run 406250 ns

#Test win cagnotte
force -freeze sim:/testLCDTop/NextCagnotte_P1 00110111 0
force -freeze sim:/testLCDTop/NextCagnotte_P2 00011001 0
force -freeze sim:/testLCDTop/NextJackPot 0000000 0
force -freeze sim:/testLCDTop/Dice_1 110 0
force -freeze sim:/testLCDTop/Dice_2 101 0
force -freeze sim:/testLCDTop/Dice_Effect 00001111 0
run 406250 ns

#=========================================
#		Test Top: Etat Player2
#=========================================
force -freeze sim:/testLCDTop/Etat_System 100 0
force -freeze sim:/testLCDTop/Etat_Player 1 0
run 406250 ns


#=========================================
#		Test Top: Etat Finished
#=========================================
#Etat player with '1' mean player2 looses
force -freeze sim:/testLCDTop/Etat_System 111 0
force -freeze sim:/testLCDTop/Etat_Player 1 0
run 406250 ns


#=========================================
#		Test Top: rst
#=========================================
force -freeze sim:/testLCDTop/rst 1 0
run 156250 ns
