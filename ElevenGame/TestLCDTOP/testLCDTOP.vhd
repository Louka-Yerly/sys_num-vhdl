----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Yerly Louka
-- comment : 		Test all LCD bloc together
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.arr8bit_PKG.all;


entity testLCDTOP is
    Port (	clk             : in  STD_LOGIC;
			rst             : in  STD_LOGIC;
			Etat_System     : in  STD_LOGIC_VECTOR(2 downto 0);
           	Etat_Player     : in  STD_LOGIC;
        	NextCagnotte_P1 : in  STD_LOGIC_VECTOR(7 downto 0);
			NextCagnotte_P2 : in  STD_LOGIC_VECTOR(7 downto 0);
			NextJackPot     : in  STD_LOGIC_VECTOR(6 downto 0);
			Dice_1          : in  STD_LOGIC_VECTOR(2 downto 0);
			Dice_2          : in  STD_LOGIC_VECTOR(2 downto 0);
			Dice_Effect     : in  STD_LOGIC_VECTOR(7 downto 0);
			DataOUT         : out STD_LOGIC_VECTOR(7 downto 0)
         );
end testLCDTOP;

architecture Arch_testLCDTOP of testLCDTOP is
	signal Str_Top    : arr8bit(0 to 5);
	signal Str_Bottom : arr8bit(0 to 4);
	signal LCD_RS     : STD_LOGIC;
	signal LCD_RW     : STD_LOGIC;
	signal LCD_E      : STD_LOGIC;
	signal LCD_Data   : arr8bit(0 to 31);
begin

	Ent1 : entity work.LCD_Top(Arch_LCD_Top)
			port map (
				clk => clk, rst => rst, Etat_System => Etat_System, NextCagnotte_P1 => NextCagnotte_P1, NextCagnotte_P2 => NextCagnotte_P2,
				NextJackPot => NextJackPot, Str_Top => Str_Top
			);
	Ent2 : entity work.LCD_Bottom(Arch_LCD_Bottom)
			port map (
				clk => clk, rst => rst, Etat_System => Etat_System, Dice_1 => Dice_1, Dice_2 => Dice_2, Dice_Effect => Dice_Effect,
				Str_Bottom => Str_Bottom
			);

	Ent3 : entity work.LCD_Concatenation(Arch_LCD_Concatenation)
			port map (
				clk => clk, rst => rst, Etat_System => Etat_System, Etat_Player => Etat_Player, Str_Top => Str_Top, Str_Bottom => Str_Bottom,
				LCD_RS => LCD_RS, LCD_RW => LCD_RW, LCD_E => LCD_E, LCD_Data => LCD_Data

			);

	DataOUT <= LCD_Data(0);
end Arch_testLCDTOP;