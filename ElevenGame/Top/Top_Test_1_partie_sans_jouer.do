restart
force -freeze sim:/top/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/top/rst 1 0
run 100 ns
force -freeze sim:/top/rst 0 0
run 100 ns
force -freeze sim:/top/btn_sw 0 0
force -freeze sim:/top/btn_push 0 0
run 1 sec
force -freeze sim:/top/btn_sw 1 0
run 40 sec