----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Haymoz Bryan
-- comment : Code for Top
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.arr8bit_PKG.all;

entity Top is
  port (Btn_Sw : in std_logic;
  	    Btn_Push : in std_logic;
	      clk : in std_logic;
	      rst : in std_logic;

	      Led_P1 : out std_logic;
	      Led_P2 : out std_logic;
	      Frequency_Buzzer : out std_logic;
	      LCD_RS : out std_logic;
	      LCD_RW : out std_logic;
	      LCD_E : out std_logic;
	      LCD_Data : out STD_LOGIC_VECTOR(7 downto 0);
        dice1 : out std_logic_vector(2 downto 0);
        dice2 : out std_logic_vector(2 downto 0));
end entity ; -- Main

architecture Arch_Top of Top is

  signal sign_Etat_1Hz : std_logic;
  signal sign_Etat_4Hz : std_logic;
  signal sign_Etat_System : std_logic_vector(2 downto 0);
  signal sign_Etat_Timer : std_logic;
  signal sign_Etat_Timer_M1 : std_logic;
  signal sign_Etat_Player : std_logic;
  signal sign_Random_1 : std_logic_vector(2 downto 0);
  signal sign_Random_2 : std_logic_vector(2 downto 0);
  signal sign_Str_Top : arr8bit(0 to 5);
  signal sign_Str_Bottom : arr8bit(0 to 4);
  signal sign_Is_Finish : std_logic;
  signal sign_Dice_1 : std_logic_vector(2 downto 0);
  signal sign_Dice_2 : std_logic_vector(2 downto 0);
  signal sign_Dice_Effect : std_logic_vector(7 downto 0);
  signal sign_NextCagnotte_P1, sign_CurrentCagnotte_P1 : std_logic_vector(7 downto 0);
  signal sign_NextCagnotte_P2, sign_CurrentCagnotte_P2 : std_logic_vector(7 downto 0);
  signal sign_NextJackpot, sign_CurrentJackpot : std_logic_vector(6 downto 0);

begin

  dice1 <= sign_Dice_1;
  dice2 <= sign_Dice_2;

  EntWork_Main : entity work.Main(Arch_Main)
    port map(clk => clk,
             rst => rst,
             Btn_Sw => Btn_Sw,
             Btn_Push => Btn_Push,
             Etat_Timer => sign_Etat_Timer,
             Is_Finish => sign_Is_Finish,
             Etat_Player => sign_Etat_Player,
             Etat_System => sign_Etat_System);

  EntWork_Impulse_4hz : entity work.Impulse_4Hz(Arch_Impulse_4Hz)
    port map(clk => clk,
             rst => rst,
             Etat_System => sign_Etat_System,
             Etat_4Hz => sign_Etat_4Hz);

    EntWork_Timer : entity work.Timer(Arch_Timer)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               Etat_1Hz => sign_Etat_1Hz,
               Etat_Timer => sign_Etat_Timer,
               Etat_Timer_M1 => sign_Etat_Timer_M1);

    EntWork_Current_Player : entity work.Current_Player(Arch_Current_Player)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               Etat_Player => sign_Etat_Player);

    EntWork_LED : entity work.LED(Arch_LED)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               Led_P1 => Led_P1,
               Led_P2 => Led_P2);

    EntWork_Random_Dice1 : entity work.Random_Dice1(Arch_Random_Dice1)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               Random_1 => sign_Random_1);

    EntWork_Random_Dice2 : entity work.Random_Dice2(Arch_Random_Dice2)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               Random_2 => sign_Random_2);

    EntWork_LCD_Top : entity work.LCD_Top(Arch_LCD_Top)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               NextCagnotte_P1 => sign_NextCagnotte_P1,
               NextCagnotte_P2 => sign_NextCagnotte_P2,
               NextJackpot => sign_NextJackpot,
               Str_Top => sign_Str_Top);

    EntWork_LCD_Bottom : entity work.LCD_Bottom(Arch_LCD_Bottom)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               Dice_1 => sign_Dice_1,
               Dice_2 => sign_Dice_2,
               Dice_Effect => sign_Dice_Effect,
               Str_Bottom => sign_Str_Bottom);

    EntWork_LCD_Concatenation : entity work.LCD_Concatenation(Arch_LCD_Concatenation)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               Etat_Player => sign_Etat_Player,
               Str_Top => sign_Str_Top,
               Str_Bottom => sign_Str_Bottom,
               LCD_RS => LCD_RS,
               LCD_RW => LCD_RW,
               LCD_E => LCD_E,
               LCD_Data => LCD_Data);

    EntWork_Points_Calculation : entity work.Points_Calculation(Arch_Points_Calculation)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               Etat_Player => sign_Etat_Player,
               Etat_Timer => sign_Etat_Timer,
               Random_1 => sign_Random_1,
               Random_2 => sign_Random_2,
               CurrentCagnotte_P1 => sign_CurrentCagnotte_P1,
               CurrentCagnotte_P2 => sign_CurrentCagnotte_P2,
               CurrentJackpot => sign_CurrentJackpot,
               NextCagnotte_P1 => sign_NextCagnotte_P1,
               NextCagnotte_P2 => sign_NextCagnotte_P2,
               NextJackpot => sign_NextJackpot,
               Dice_1 => sign_Dice_1,
               Dice_2 => sign_Dice_2,
               Dice_effect => sign_Dice_Effect,
               Is_Finish => sign_Is_Finish);

    EntWork_Points_Stockage : entity work.Points_Stockage(Arch_Points_Stockage)
      port map(clk => clk,
               rst => rst,
               CurrentCagnotte_P1 => sign_CurrentCagnotte_P1,
               CurrentCagnotte_P2 => sign_CurrentCagnotte_P2,
               CurrentJackpot => sign_CurrentJackpot,
               Etat_System => sign_Etat_System,
               NextCagnotte_P1 => sign_NextCagnotte_P1,
               NextCagnotte_P2 => sign_NextCagnotte_P2,
               NextJackpot => sign_NextJackpot);

    EntWork_Buzzer : entity work.Buzzer(Arch_Buzzer)
      port map(clk => clk,
               rst => rst,
               Etat_System => sign_Etat_System,
               Etat_Timer_M1 => sign_Etat_Timer_M1,
               Etat_Timer => sign_Etat_Timer,
               Etat_4Hz => sign_Etat_4Hz,
               Frequency_Buzzer => Frequency_Buzzer);


   EntWork_Impulse_1Hz : entity work.Impulse_1Hz(Arch_Impulse_1Hz)
      port map(clk => clk,
              rst => rst,
              Etat_System => sign_Etat_System,
              Etat_1Hz => sign_Etat_1Hz);

end architecture ; -- arch
