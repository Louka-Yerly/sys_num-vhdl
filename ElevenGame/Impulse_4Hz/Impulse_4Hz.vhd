----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Schroeter Maxime
-- comment : 		A comment example
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Impulse_4Hz is
	port (clk: IN std_logic;
		rst: IN std_logic;
		Etat_System: IN std_logic_vector(2 downto 0);
		Etat_4Hz: OUT std_logic);
end entity Impulse_4Hz;

architecture Arch_Impulse_4Hz of Impulse_4Hz is
	constant maxVal: unsigned(13 downto 0):= to_unsigned(16000, 14);
	signal compteur_present, compteur_futur : unsigned(13 downto 0);
	signal fin_compteur: std_logic;
	signal compte: std_logic;
begin
	registre: process(clk, rst) is
	begin
		if rst = '1' then
			compteur_present <= (others =>'0');
		elsif rising_edge(clk) then
			compteur_present <= compteur_futur;
		end if;
	end process;

	compte <= '1' when (Etat_System = "111" or Etat_System = "100" or Etat_System = "010" or Etat_System = "001") else
						'0';

	fin_compteur <= '1' when compte = '1' and (compteur_present = maxVal) else
									'0';
	process(fin_compteur, compteur_present, compte) is
	begin
		if compte = '1' and (fin_compteur='0')  then
			compteur_futur <= compteur_present+1;
		else
			compteur_futur <= (others =>'0');
		end if;
	end process;

	Etat_4Hz <= fin_compteur;

end architecture;
