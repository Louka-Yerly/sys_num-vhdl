restart
force -freeze sim:/impulse_4hz/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/impulse_4hz/rst 1 0
force -freeze sim:/impulse_4hz/etat_system 000 0
run 100 ns

force -freeze sim:/impulse_4hz/rst 0 0
run 100 ns

force -freeze sim:/impulse_4hz/etat_system 100 0
run 1000 ms

force -freeze sim:/impulse_4hz/etat_system 000 0
run 2000 ms