#=========================================
#				Initialisation
#=========================================
force -freeze sim:/LCD_Top/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/LCD_Top/rst 1 0
force -freeze sim:/LCD_Top/CurrentCagnotte_P1 00101000 0
force -freeze sim:/LCD_Top/CurrentCagnotte_P2 00101000 0
force -freeze sim:/LCD_Top/CurrentJackPot 0000000 0
force -freeze sim:/LCD_Top/Etat_System 000 0
run 100 ns
force -freeze sim:/LCD_Top/rst 0 0
run 31250 ns

#=========================================
#		Test Top: Etat Off
#=========================================
force -freeze sim:/LCD_Top/Etat_System 000 0
run 156250 ns
 
#=========================================
#		Test Top: Etat Player1
#=========================================
force -freeze sim:/LCD_Top/Etat_System 001 0
run 406250

#=========================================
#		Test Top: Etat Waiting
#=========================================
#36 points in Player1 cagnotte and 4 points in Jackpot 
force -freeze sim:/LCD_Top/Etat_System 010 0
run 31250 ns
force -freeze sim:/LCD_Top/CurrentCagnotte_P1 00100100 0
force -freeze sim:/LCD_Top/CurrentCagnotte_P2 00101000 0
force -freeze sim:/LCD_Top/CurrentJackPot 0000100 0
run 31250 ns

#80 points in jackpot
force -freeze sim:/LCD_Top/Etat_System 010 0
run 31250 ns
force -freeze sim:/LCD_Top/CurrentCagnotte_P1 00000000 0
force -freeze sim:/LCD_Top/CurrentCagnotte_P2 00000000 0
force -freeze sim:/LCD_Top/CurrentJackPot 1010000 0
run 31250 ns

#80 points in Player1's cagnotte
force -freeze sim:/LCD_Top/Etat_System 010 0
run 31250 ns
force -freeze sim:/LCD_Top/CurrentCagnotte_P1 01010000 0
force -freeze sim:/LCD_Top/CurrentCagnotte_P2 00000000 0
force -freeze sim:/LCD_Top/CurrentJackPot 0000000 0
run 31250 ns


#=========================================
#		Test Top: Etat Player2
#=========================================
force -freeze sim:/LCD_Top/Etat_System 100 0
run 406250 ns

#=========================================
#		Test Top: Etat Finished
#=========================================
force -freeze sim:/LCD_Top/Etat_System 111 0
run 156250 ns

#=========================================
#		Test Top: rst
#=========================================
force -freeze sim:/LCD_Top/rst 1 0
run 156250 ns
