----------------------------------------------------------------------------------
-- Group number:  5
-- Group member:  Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name: Yerly Louka
-- comment :    Code for LCD_Top
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.arr8bit_PKG.all;

entity LCD_Top is
    Port ( clk              : in  STD_LOGIC;
           rst              : in  STD_LOGIC;
           Etat_System      : in  STD_LOGIC_VECTOR(2 downto 0);
           NextCagnotte_P1  : in  STD_LOGIC_VECTOR(7 downto 0);
           NextCagnotte_P2  : in  STD_LOGIC_VECTOR(7 downto 0);
           NextJackpot      : in  STD_LOGIC_VECTOR(6 downto 0);
           Str_Top          : out arr8bit(0 to 5));
end LCD_Top;

architecture Arch_LCD_Top of LCD_Top is
-- Constant
	constant data_09  : arr8bit(0 to 9) := (x"30",x"31",x"32",x"33",x"34",x"35",x"36",x"37",x"38",x"39");

-- Signal
	signal valeur_presente, valeur_future : arr8bit(0 to 5);

begin
	registre : process(clk, rst)
  	begin
    	if rst = '1' then
      		valeur_presente <= (others => (others => '0'));

    	elsif rising_edge(clk) then
      		valeur_presente <= valeur_future;
    	end if;
  	end process ; -- registre

  	string_process : process(Etat_System, NextCagnotte_P1, NextCagnotte_P2, NextJackpot, valeur_presente)
    	-- NC mean NextCagnotte and NJ mean NextJackport
    	variable array_temp    	: arr8bit(0 to 5);
    	variable NC_P1          : integer range -2048 to 2047 := to_integer(signed(NextCagnotte_P1));
    	variable NC_P1_Dizaine  : integer range -2048 to 2047 := 0;
    	variable NC_P1_Unite    : integer range -2048 to 2047 := 0;
    	variable NC_P2          : integer range -2048 to 2047 := to_integer(signed(NextCagnotte_P2));
    	variable NC_P2_Dizaine  : integer range -2048 to 2047 := 0;
    	variable NC_P2_Unite    : integer range -2048 to 2047 := 0;
    	variable NJ             : integer range 0 to 2047     := to_integer(unsigned(NextJackpot));
    	variable NJ_Dizaine     : integer range 0 to 2047     := 0;
    	variable NJ_Unite       : integer range 0 to 2047     := 0;

  	begin
    	if Etat_System = "010" or Etat_System = "000" then
    	 	--Calculation of the number of Dizaine and number of Unite
	    	NC_P1 := to_integer(signed(NextCagnotte_P1));
	    	NC_P2 := to_integer(signed(NextCagnotte_P2));
	    	NJ    := to_integer(unsigned(NextJackpot));

        if NC_P1 >= 0  and NC_P2 >= 0 then
	    	      -- x*13/128 ~= x/10
	    	      NC_P1_Dizaine := (NC_P1*13)/128;
	    	      NC_P2_Dizaine := (NC_P2*13)/128;
	    	      NJ_Dizaine    := (NJ*13)/128;
	    	      NC_P1_Unite   := NC_P1-(NC_P1_Dizaine*10);
	    	      NC_P2_Unite   := NC_P2-(NC_P2_Dizaine*10);
	    	      NJ_Unite      := NJ-(NJ_Dizaine*10);

      	      -- Assignation to the corresponding ASCII code
	    	      array_temp(0) := data_09(NC_P1_Dizaine);
	    	      array_temp(1) := data_09(NC_P1_Unite);
	    	      array_temp(2) := data_09(NJ_Dizaine);
      	    	array_temp(3) := data_09(NJ_Unite);
      	    	array_temp(4) := data_09(NC_P2_Dizaine);
      	    	array_temp(5) := data_09(NC_P2_Unite);
       else
              -- Assignation to the corresponding ASCII code
              array_temp(0) := x"20";
              array_temp(1) := x"20";
              array_temp(2) := x"20";
              array_temp(3) := x"20";
              array_temp(4) := x"20";
              array_temp(5) := x"20";
        end if;
	    	valeur_future <= array_temp;
		else
			-- Nothing new to display
		    valeur_future <= valeur_presente;
		end if;
 	end process;

  	Str_Top <= valeur_presente;

end Arch_LCD_Top;
