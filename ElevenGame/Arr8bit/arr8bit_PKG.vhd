----------------------------------------------------------------------------------
-- Group number:  5
-- Group member:  Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name: Yerly Louka
-- comment :      Package used for String bloc
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package arr8bit_PKG is
  type arr8bit is array(natural range <>) of STD_LOGIC_VECTOR(7 downto 0);
end package ; -- arr8bit_PKG
package body arr8bit_PKG is
end package body ; -- body