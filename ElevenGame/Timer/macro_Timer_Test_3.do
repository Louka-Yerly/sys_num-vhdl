restart
force -freeze sim:/timer/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/timer/rst 1 0
force -freeze sim:/timer/etat_system 000 0
force -freeze sim:/timer/etat_1hz 0 0
run 100 ns

force -freeze sim:/timer/rst 0 0
run 100 ns

force -freeze sim:/timer/etat_system 100 0
force -freeze sim:/timer/etat_1hz 0 0
run 1000ms

force -freeze sim:/timer/etat_1hz 1 0
run 15625ns
force -freeze sim:/timer/etat_1hz 0 0
run 1000ms
force -freeze sim:/timer/etat_1hz 1 0
force -freeze sim:/timer/etat_system 010 0
run 15625ns
force -freeze sim:/timer/etat_1hz 0 0
run 1000ms
force -freeze sim:/timer/etat_1hz 1 0
run 15625ns
force -freeze sim:/timer/etat_1hz 0 0
run 1000ms
force -freeze sim:/timer/etat_1hz 1 0
run 15625ns
force -freeze sim:/timer/etat_1hz 0 0
run 1000ms
force -freeze sim:/timer/etat_1hz 1 0
run 15625ns
force -freeze sim:/timer/etat_1hz 0 0
run 1000ms
