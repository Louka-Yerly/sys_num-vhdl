----------------------------------------------------------------------------------
-- Group number:  5
-- Group member:  Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name: Schroeter Maxime
-- comment :    A comment example
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Timer is
	port (clk: IN std_logic;
		rst: IN std_logic;
		Etat_System: IN std_logic_vector(2 downto 0);
		Etat_1Hz: IN std_logic;
		Etat_Timer: OUT std_logic;
		Etat_Timer_M1: OUT std_logic);
end entity Timer;

architecture Arch_Timer of Timer is
	constant maxValM1: unsigned(1 downto 0):= to_unsigned(1, 2);
	constant maxVal: unsigned(1 downto 0):= to_unsigned(2, 2);
	signal compteur_present, compteur_futur : unsigned(1 downto 0);
	signal etat_present, etat_futur : std_logic_vector(2 downto 0);
	signal compte :std_logic;
	signal reinit :std_logic;

	signal fin_compteur_mem, fin_m1_mem : std_logic;

begin
	registreCompteur: process(clk, rst) is
	begin
		if rst = '1' then
			compteur_present <= (others =>'0');
		elsif rising_edge(clk) then
			compteur_present <= compteur_futur;
		end if;
	end process;

	registreEtat: process(clk, rst) is
	begin
		if rst = '1' then
			etat_present <= (others =>'0');
		elsif rising_edge(clk) then
			etat_present <= etat_futur;
		end if;
	end process;

	reinit <= '1' when Etat_System = "010" and (etat_present = "001" or etat_present="100") else
						'0';
	compte <= '1' when Etat_System = "010" or Etat_System = "001" or Etat_System = "100" else
			       '0';



	 registreNoGlitch: process(clk, rst) is
		begin
			if rst = '1' then
				fin_compteur_mem <= '0';
				fin_m1_mem <= '0';
			elsif rising_edge(clk) then
				if compte = '1' and compteur_present = maxVal and  Etat_1Hz = '1' then
					fin_compteur_mem <= '1';
				else
					fin_compteur_mem <= '0';
				end if;
				if (Etat_System = "100" or Etat_System = "001") and (compteur_present = maxValM1) and  Etat_1Hz = '1' then
					fin_m1_mem <= '1';
				else
					fin_m1_mem <= '0';
				end if;
			end if;
		end process;

	process(compteur_present, compte, Etat_1Hz, reinit) is
	begin
		if reinit ='1' then
			compteur_futur <= (others =>'0');
		elsif Etat_1Hz = '1' and compte = '1' then
			compteur_futur <= compteur_present+1;
		elsif compte = '1' then
			compteur_futur <= compteur_present;
		else
			compteur_futur <= (others =>'0');
		end if;
	end process;

	process(Etat_System) is
	begin
		etat_futur <= Etat_System;
	end process;

	Etat_Timer <= fin_compteur_mem;
	Etat_Timer_M1 <= fin_m1_mem;


end architecture;
