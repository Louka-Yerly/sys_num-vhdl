restart
force -freeze sim:/frequence/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/frequence/rst 1 0
force -freeze sim:/frequence/choix_freq 00 0
run 100 ns

force -freeze sim:/frequence/rst 0 0
run 100 ns

force -freeze sim:/frequence/choix_freq 01 0
run 1000 ms

force -freeze sim:/frequence/choix_freq 00 0
run 1000 ms

force -freeze sim:/frequence/choix_freq 10 0
run 1000 ms