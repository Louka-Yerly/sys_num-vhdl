restart
force -freeze sim:/duration/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/duration/rst 1 0
force -freeze sim:/duration/choix_duree 00 0
run 100 ns

force -freeze sim:/duration/rst 0 0
run 100 ns

force -freeze sim:/duration/choix_duree 10 0
run 2000 ms

force -freeze sim:/duration/choix_duree 00 0
run 2000 ms

force -freeze sim:/duration/choix_duree 01 0
run 2000 ms