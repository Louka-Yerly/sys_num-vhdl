----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Schroeter Maxime
-- comment : 		A comment example
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Duration is
	port (clk: IN std_logic;
		rst: IN std_logic;
		choix_duree: IN std_logic_vector(1 downto 0);
		fin_duree: OUT std_logic);
end entity Duration;

architecture Arch_Duration of Duration is
	constant maxValDuree150: unsigned(14 downto 0):= to_unsigned(9600, 15);
	constant maxValDuree500: unsigned(14 downto 0):= to_unsigned(32000, 15);
	signal compteur_present, compteur_futur : unsigned(14 downto 0);
	signal fin_compteur: std_logic;
begin
	registre: process(clk, rst) is
	begin
		if rst = '1' then
			compteur_present <= (others =>'0');
		elsif rising_edge(clk) then
			compteur_present <= compteur_futur;
		end if;
	end process;

	fin_compteur <= '1' when (compteur_present = maxValDuree500 and choix_duree="10") or (compteur_present = maxValDuree150 and choix_duree="01") else
					'0';
	process(fin_compteur, compteur_present, choix_duree) is
	begin
		if choix_duree="00" or choix_duree="11" or fin_compteur='1'  then
			compteur_futur <= (others =>'0');
		else
			compteur_futur <= compteur_present+1;
		end if;
	end process;

	fin_duree <= fin_compteur;

end architecture;
