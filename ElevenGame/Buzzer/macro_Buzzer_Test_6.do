restart
force -freeze sim:/buzzer/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/buzzer/rst 1 0
force -freeze sim:/buzzer/etat_timer 0 0
force -freeze sim:/buzzer/etat_timer_m1 0 0
force -freeze sim:/buzzer/etat_4hz 0 0
force -freeze sim:/buzzer/etat_system 000 0
run 1 ms

force -freeze sim:/buzzer/rst 0 0
run 1 ms

force -freeze sim:/buzzer/etat_system 100 0
run 100 ms


force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_4hz 0 0
run 250 ms

force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_4hz 0 0
run 250 ms

force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_timer_m1 1 0
run 15625 ns
force -freeze sim:/buzzer/etat_timer_m1 0 0
force -freeze sim:/buzzer/etat_4hz 0 0
run 250 ms


force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_4hz 0 0
run 250 ms

force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_4hz 0 0
run 200 ms

force -freeze sim:/buzzer/etat_system 010 0
run 50 ms

force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_4hz 0 0
run 250 ms

force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_4hz 0 0
run 250 ms

force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_4hz 0 0
run 250 ms

force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_4hz 0 0
run 250 ms

force -freeze sim:/buzzer/etat_4hz 1 0
run 15625 ns

force -freeze sim:/buzzer/etat_4hz 0 0
run 250 ms
