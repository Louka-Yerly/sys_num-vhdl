----------------------------------------------------------------------------------
-- Group number:  5
-- Group member:  Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name: Schroeter Maxime
-- comment :    A comment example
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity Frequence is
	port (clk: IN std_logic;
		rst: IN std_logic;
		choix_freq: IN std_logic_vector(1 downto 0);
		frequency_buzzer: OUT std_logic);
end entity Frequence;


architecture Arch_Frequence of Frequence is
		constant maxValFreq200: unsigned(7 downto 0) := to_unsigned(159, 8); --159
  	constant maxValFreq600: unsigned(7 downto 0) := to_unsigned(52, 8); --52
  	signal compteur_present, compteur_futur : unsigned(7 downto 0);
  	Signal freq_present, freq_future: std_logic;
		signal fin_compteur: std_logic;

  begin
  	registre: process(clk, rst) is
  		begin
  			if rst = '1' then
  			compteur_present <= (others =>'0');
  			elsif rising_edge(clk) then
  				compteur_present <= compteur_futur;
  			end if;
  	end process;

  	fin_compteur <= '1' when (compteur_present = maxValFreq200 and choix_freq="01") or (compteur_present = maxValFreq600 and choix_freq="10")else
										'0';



  	process(fin_compteur, compteur_present, choix_freq)  is
  	begin
  		case choix_freq is
  			when "00" =>
  				compteur_futur <= (others => '0');
  			when "01" | "10" =>
  				if fin_compteur = '1' then
  					compteur_futur<=(others=>'0');
  				else
  					compteur_futur<=compteur_present+1;
  				end if;
  			when others =>
  				compteur_futur <= (others =>'0');
  		end case;
  	end process;


  	process(fin_compteur, freq_present) is
  		begin
  		if fin_compteur = '1' then
  			freq_future <= not freq_present;
  		else
  			freq_future <= freq_present;
  		end if;
  	end process;

  	registre2: process(clk, rst) is
  		begin
  		if rst='1' then
  			freq_present <= '0';
  		elsif rising_edge(clk) then
  			freq_present <= freq_future;
  		end if;
  	end process;

  	frequency_buzzer <= freq_present;

end architecture;
