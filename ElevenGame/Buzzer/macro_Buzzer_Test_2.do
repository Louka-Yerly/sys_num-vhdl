restart
force -freeze sim:/buzzer/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/buzzer/rst 1 0
force -freeze sim:/buzzer/etat_timer 0 0
force -freeze sim:/buzzer/etat_timer_m1 0 0
force -freeze sim:/buzzer/etat_4hz 0 0
force -freeze sim:/buzzer/etat_system 000 0
run 1 ms

force -freeze sim:/buzzer/rst 0 0
run 1 ms

force -freeze sim:/buzzer/etat_system 111 0
run 1000 ms

force -freeze sim:/buzzer/etat_system 000 0
run 200 ms
