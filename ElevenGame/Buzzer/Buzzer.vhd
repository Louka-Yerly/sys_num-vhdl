----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Schroeter Maxime
-- comment : 		A comment example
----------------------------------------------------------------------------------
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.numeric_std.all ;


entity Buzzer is
  port (clk: IN std_logic;
		rst: IN std_logic;
    Etat_Timer: IN std_logic;
		Etat_Timer_M1: IN std_logic;
    Etat_4Hz: IN std_logic;
    Etat_System: IN std_logic_vector(2 downto 0);
    frequency_buzzer: OUT std_logic);
end entity Buzzer;

architecture Arch_Buzzer of Buzzer is
  TYPE etat IS (Off, FinTimer, Victoire, FinTimerM1, Bip1, Bip2, Bip3, Bip4, PauseBip1, PauseBip2, PauseBip3, FinVictoire);
  SIGNAL etat_present, etat_futur : etat;
  SIGNAL choix_duree: std_logic_vector(1 downto 0);
  SIGNAL choix_freq: std_logic_vector(1 downto 0);
  SIGNAL fin_duree: std_logic;
begin
  Duree: entity work.Duration(Arch_Duration)
        port map (
          clk => clk,
  		    rst => rst,
  		    choix_duree => choix_duree,
  		    fin_duree => fin_duree);

  Frequence: entity work.Frequence(Arch_Frequence)
             port map (
             clk => clk,
             rst => rst,
             choix_freq => choix_freq,
             frequency_buzzer => frequency_buzzer);

	registre : process(clk, rst)
	begin
		if rst = '1' then
			etat_present <= Off;
		elsif rising_edge(clk) then
			etat_present <= etat_futur;
		end if;
	end process registre;

  choix_duree <= "01" when etat_present=Bip1 or etat_present=Bip2 or etat_present=Bip3 or etat_present=Bip4 else
                 "10" when etat_present=FinTimer or etat_present=Victoire else
                 "00";

  choix_freq <= "10" when etat_present=Bip1 or etat_present=Bip2 or etat_present=Bip3 or etat_present=Bip4 else
                "01" when etat_present=FinTimer or etat_present=Victoire else
                "00";

  process(etat_present,Etat_System, fin_duree, Etat_4Hz, Etat_Timer, Etat_Timer_M1) is
  begin
    case(etat_present) is
      when Off =>
        if Etat_System="111" then
          etat_futur <= Victoire;
        elsif (Etat_System="001" or Etat_System="100") and Etat_Timer='1' then
          etat_futur <= FinTimer;
        elsif (Etat_System="001" or Etat_System="100") and Etat_Timer_M1='1' then
          etat_futur <= FinTimerM1;
        else
          etat_futur <= Off;
        end if;
      when victoire =>
        if fin_duree='1' then
          etat_futur <= FinVictoire;
        else
          etat_futur <= Victoire;
        end if;
      when FinVictoire =>
        if Etat_System="000" then
          etat_futur <= Off;
        else
          etat_futur <= FinVictoire;
        end if;
      when FinTimer =>
        if fin_duree='1' then
          etat_futur <= Off;
        else
          etat_futur <= FinTimer;
        end if;
      when FinTimerM1 =>
        if Etat_System="010" then
          etat_futur <= Off;
        elsif Etat_4Hz='1' then
            etat_futur <= Bip1;
        else
            etat_futur <= FinTimerM1;
        end if;
      when Bip1 =>
        if Etat_System="010" then
          etat_futur <= Off;
        elsif fin_duree='1' then
          etat_futur <= PauseBip1;
        else
          etat_futur <= Bip1;
        end if;
      when PauseBip1 =>
        if Etat_System="010" then
          etat_futur <= Off;
        elsif Etat_4Hz='1' then
            etat_futur <= Bip2;
        else
            etat_futur <= PauseBip1;
        end if;
      when Bip2 =>
        if Etat_System="010" then
          etat_futur <= Off;
        elsif fin_duree='1' then
          etat_futur <= PauseBip2;
        else
          etat_futur <= Bip2;
        end if;
      when PauseBip2 =>
        if Etat_System="010" then
          etat_futur <= Off;
        elsif Etat_4Hz='1' then
            etat_futur <= Bip3;
        else
            etat_futur <= PauseBip2;
        end if;
      when Bip3 =>
        if Etat_System="010" then
          etat_futur <= Off;
        elsif fin_duree='1' then
          etat_futur <= PauseBip3;
        else
          etat_futur <= Bip3;
        end if;
      when PauseBip3 =>
        if Etat_System="010" then
          etat_futur <= Off;
        elsif Etat_4Hz='1' then
            etat_futur <= Bip4;
        else
            etat_futur <= PauseBip3;
        end if;
      when Bip4 =>
        if Etat_System="010" then
          etat_futur <= Off;
        elsif fin_duree='1' then
          etat_futur <= Off;
        else
          etat_futur <= Bip4;
        end if;
      when others =>
        etat_futur <= off;
    end case;
  end process;

end architecture Arch_Buzzer;
