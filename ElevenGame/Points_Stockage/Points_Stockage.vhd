----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Haymoz Bryan
-- comment : Code for Points_Stockage
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Points_Stockage is
    Port (clk : in std_logic;
          rst : in std_logic;
          CurrentCagnotte_P1 : out  STD_LOGIC_VECTOR (7 downto 0);
          CurrentCagnotte_P2 : out  STD_LOGIC_VECTOR (7 downto 0);
          CurrentJackpot : out  STD_LOGIC_VECTOR (6 downto 0);
          Etat_System : in STD_LOGIC_VECTOR (2 downto 0);
          NextCagnotte_P1 : in  STD_LOGIC_VECTOR (7 downto 0);
          NextCagnotte_P2 : in  STD_LOGIC_VECTOR (7 downto 0);
          NextJackpot : in  STD_LOGIC_VECTOR (6 downto 0));
end Points_Stockage;

architecture Arch_Points_Stockage of Points_Stockage is

  signal temp_CurrentCagnotte_P1, temp_CurrentCagnotte_P2, temp_NextCagnotte_P1, temp_NextCagnotte_P2 :  std_logic_vector(7 downto 0);
  signal temp_CurrentJackpot, temp_NextJackpot : std_logic_vector(6 downto 0);

begin

  registre : process(clk, rst)
  begin
    if rst = '1' then
        temp_CurrentCagnotte_P1 <= (others => '0');
        temp_CurrentCagnotte_P2 <= (others => '0');
        temp_CurrentJackpot <= (others => '0');
    elsif rising_edge(clk) then -- clock flanc montant
        temp_CurrentCagnotte_P1 <= temp_NextCagnotte_P1;
        temp_CurrentCagnotte_P2 <= temp_NextCagnotte_P2;
        temp_CurrentJackpot <=  temp_NextJackpot;
    end if;
  end process registre;

  combi : process(Etat_System, NextCagnotte_P1, NextCagnotte_P2, NextJackpot,  temp_CurrentCagnotte_P1,  temp_CurrentCagnotte_P2,  temp_CurrentJackpot)
  begin
      temp_NextCagnotte_P1 <= NextCagnotte_P1;
      temp_NextCagnotte_P2 <= NextCagnotte_P2;
      temp_NextJackpot <= NextJackpot;
  end process combi;

      CurrentCagnotte_P1 <= temp_CurrentCagnotte_P1;
      CurrentCagnotte_P2 <= temp_CurrentCagnotte_P2;
      CurrentJackpot <=  temp_CurrentJackpot;

end Arch_Points_Stockage;
