restart

#Test Player2
force -freeze sim:/points_stockage/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/points_stockage/rst 1 0
force -freeze sim:/points_stockage/etat_system 100 0
force -freeze sim:/points_stockage/nextcagnotte_p1 00101000 0
force -freeze sim:/points_stockage/nextcagnotte_p2 00101000 0
force -freeze sim:/points_stockage/nextjackpot 0000000 0
run 15625 ns
force -freeze sim:/points_stockage/rst 0 0
run 15625 ns
run 15625 ns
run 15625 ns

#Test Waiting
force -freeze sim:/points_stockage/etat_system 010 0
force -freeze sim:/points_stockage/nextcagnotte_p1 00001000 0
force -freeze sim:/points_stockage/nextcagnotte_p2 00001000 0
force -freeze sim:/points_stockage/nextjackpot 1000000 0
run 15625 ns
run 15625 ns
run 15625 ns
run 15625 ns

#Test Player2
force -freeze sim:/points_stockage/etat_system 100 0
force -freeze sim:/points_stockage/nextcagnotte_p1 00001000 0
force -freeze sim:/points_stockage/nextcagnotte_p2 00001000 0
force -freeze sim:/points_stockage/nextjackpot 1000000 0
run 15625 ns
run 15625 ns
run 15625 ns
run 15625 ns