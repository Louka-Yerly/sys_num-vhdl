----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:  	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name: 	Yerly Louka
-- comment :    	Code for LCD_Bottom
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.arr8bit_PKG.all;


entity LCD_Bottom is
	Port ( clk         : in  STD_LOGIC;
		   rst         : in  STD_LOGIC;
		   Etat_System : in  STD_LOGIC_VECTOR(2 downto 0);
		   Dice_1      : in  STD_LOGIC_VECTOR(2 downto 0);
		   Dice_2      : in  STD_LOGIC_VECTOR(2 downto 0);
		   Dice_Effect : in  STD_LOGIC_VECTOR(7 downto 0);
		   Str_Bottom  : out arr8bit(0 to 4));
end LCD_Bottom;

architecture Arch_LCD_Bottom of LCD_Bottom is
--Constant
		constant data_09      : arr8bit(0 to 9) := (x"30",x"31",x"32",x"33",x"34",x"35",x"36",x"37",x"38",x"39");
		constant data_15      : arr8bit(0 to 4) := (x"30", x"30", x"2D", x"31", x"35");
		constant data_Special : arr8bit(0 to 1) := (x"2B",x"2D");

--Signal
		signal valeur_presente, valeur_future : arr8bit(0 to 4);

begin
		registre : process(clk, rst)
		begin
				if rst = '1' then
						valeur_presente <= (others => (others => '0'));

				elsif rising_edge(clk) then
						valeur_presente <= valeur_future;
				end if;
		end process ; -- registre

		string_process : process(Etat_System, Dice_1, Dice_2, Dice_Effect, valeur_presente)
				variable valeur_temp         : arr8bit(0 to 4);
				variable Dice_1_Integer      : integer range 0 to 8        := to_integer(unsigned(Dice_1));
				variable Dice_2_Integer      : integer range 0 to 8        := to_integer(unsigned(Dice_2));
				variable Dice_Effect_Integer : integer range -2048 to 2047 := to_integer(signed(Dice_Effect));
				variable Dice_Effect_Dizaine : integer range -2048 to 2047 := 0;
				variable Dice_Effect_Unite   : integer range -2048 to 2047 := 0;

		begin
				case Etat_System is
						when "010"  => --Waiting
								Dice_1_Integer      := to_integer(unsigned(Dice_1));
								Dice_2_Integer      := to_integer(unsigned(Dice_2));
								Dice_Effect_Integer := to_integer(signed(Dice_Effect));


								if Dice_Effect_Integer = -15 then
									-- The player didn't play
									valeur_future <= data_15;

								else
										if Dice_Effect_Integer < 0 then
												-- Negatif impact
												valeur_temp(2) := data_Special(1);
										else
												-- Positif impact
												valeur_temp(2) := data_Special(0);
										end if ;

									-- We already know the sign
										Dice_Effect_Integer := to_integer(abs(signed(Dice_Effect)));

									-- x*13/128 ~= x/10
									Dice_Effect_Dizaine := (Dice_Effect_Integer*13)/128;
									Dice_Effect_Unite   := Dice_Effect_Integer - (Dice_Effect_Dizaine*10);

									-- Assignation to the corresponding ASCII code
									valeur_temp(0) := data_09(Dice_1_Integer);
									valeur_temp(1) := data_09(Dice_2_Integer);
									valeur_temp(3) := data_09(Dice_Effect_Dizaine);
									valeur_temp(4) := data_09(Dice_Effect_Unite);

									valeur_future <= valeur_temp;

								end if ;
						when others => -- Others state
							-- Nothing new to Display
						 valeur_future <= valeur_presente;
				end case;
		end process;

		Str_Bottom <= valeur_presente;

end Arch_LCD_Bottom;
