#=========================================
#				Initialisation
#=========================================
force -freeze sim:/LCD_Bottom/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/LCD_Bottom/rst 1 0
force -freeze sim:/LCD_Bottom/Etat_System 000 0
force -freeze sim:/LCD_Bottom/Dice_Effect 00000000 0
force -freeze sim:/LCD_Bottom/Dice_1 000 0
force -freeze sim:/LCD_Bottom/Dice_2 000 0
run 100 ns
force -freeze sim:/LCD_Bottom/rst 0 0
run 100 ns

#=========================================
#		Test Bottom: Etat Off
#=========================================
force -freeze sim:/LCD_Bottom/Etat_System 000 0
run 156250 ns
 
#=========================================
#		Test Bottom: Etat Player1
#=========================================
force -freeze sim:/LCD_Bottom/Etat_System 001 0
run 406250


#=========================================
#		Test Bottom: Etat Waiting
#=========================================
#Effect -6
force -freeze sim:/LCD_Bottom/Etat_System 010 0
force -freeze sim:/LCD_Bottom/Dice_Effect 11111010 0
force -freeze sim:/LCD_Bottom/Dice_1 010 0
force -freeze sim:/LCD_Bottom/Dice_2 011 0
run 31250 ns

#Effect -15 Player didn't play
force -freeze sim:/LCD_Bottom/Etat_System 010 0
force -freeze sim:/LCD_Bottom/Dice_Effect 11110001 0
force -freeze sim:/LCD_Bottom/Dice_1 000 0
force -freeze sim:/LCD_Bottom/Dice_2 000 0
run 31250 ns

#Win gagnotte 39 points
force -freeze sim:/LCD_Bottom/Etat_System 010 0
force -freeze sim:/LCD_Bottom/Dice_Effect 00100111 0
force -freeze sim:/LCD_Bottom/Dice_1 110 0
force -freeze sim:/LCD_Bottom/Dice_2 101 0
run 31250 ns

#Extrem: win 80 points
force -freeze sim:/LCD_Bottom/Etat_System 010 0
force -freeze sim:/LCD_Bottom/Dice_Effect 01010000 0
force -freeze sim:/LCD_Bottom/Dice_1 110 0
force -freeze sim:/LCD_Bottom/Dice_2 101 0
run 31250 ns

#Extrem: player looses 12 points
force -freeze sim:/LCD_Bottom/Etat_System 010 0
force -freeze sim:/LCD_Bottom/Dice_Effect 11110100 0
force -freeze sim:/LCD_Bottom/Dice_1 110 0
force -freeze sim:/LCD_Bottom/Dice_2 110 0
run 31250 ns

#=========================================
#		Test Bottom: Etat Player2
#=========================================
force -freeze sim:/LCD_Bottom/Etat_System 100 0
run 406250 ns

#=========================================
#		Test Bottom: rst
#=========================================
force -freeze sim:/LCD_Bottom/rst 1 0
run 156250 ns
