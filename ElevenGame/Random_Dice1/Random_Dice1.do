#=========================================
#				Initialisation
#=========================================
force -freeze sim:/Random_Dice1/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/Random_Dice1/rst 1 0
force -freeze sim:/Random_Dice1/Etat_System 000 0
run 100 ns
force -freeze sim:/Random_Dice1/rst 0 0
run 100 ns

#=========================================
#		Test Dice: Etat Off
#=========================================
force -freeze sim:/Random_Dice1/Etat_System 000 0
run 156250 ns
 
#=========================================
#		Test Dice: Etat Player1
#=========================================
force -freeze sim:/Random_Dice1/Etat_System 001 0
run 406250 ns (pour voir le reload)

#=========================================
#		Test Dice: Etat Waiting
#=========================================
force -freeze sim:/Random_Dice1/Etat_System 010 0
run 156250 ns

#=========================================
#		Test Dice: Etat Player2
#=========================================
force -freeze sim:/Random_Dice1/Etat_System 100 0
run 406250 ns

#=========================================
#		Test Dice: Etat Finished
#=========================================
force -freeze sim:/Random_Dice1/Etat_System 111 0
run 156250 ns

#=========================================
#		Test Dice: rst
#=========================================
force -freeze sim:/Random_Dice1/rst 1 0
run 156250 ns