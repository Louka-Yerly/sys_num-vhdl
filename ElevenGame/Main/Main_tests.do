restart

#Etat Off vers Player1
force -freeze sim:/main/btn_sw 0 0
force -freeze sim:/main/btn_push 0 0
force -freeze sim:/main/etat_timer 0 0
force -freeze sim:/main/is_finish 0 0
force -freeze sim:/main/etat_player 0 0
force -freeze sim:/main/rst 0 0
force -freeze sim:/main/clk 0 0
run
force -freeze sim:/main/btn_sw 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Player1 vers Off
force -freeze sim:/main/btn_sw 0 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Off vers Player1
force -freeze sim:/main/btn_sw 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Player1 vers Waiting
force -freeze sim:/main/btn_push 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Waiting vers Off
force -freeze sim:/main/btn_sw 0 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Off vers Player1
force -freeze sim:/main/btn_push 0 0
force -freeze sim:/main/btn_sw 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Player1 vers Waiting
force -freeze sim:/main/btn_push 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Waiting vers Player2
force -freeze sim:/main/btn_push 0 0
force -freeze sim:/main/etat_player 1 0
force -freeze sim:/main/etat_timer 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Player2 vers Off
force -freeze sim:/main/etat_player 0 0
force -freeze sim:/main/etat_timer 0 0
force -freeze sim:/main/btn_sw 0 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Off vers Player1
force -freeze sim:/main/btn_push 0 0
force -freeze sim:/main/btn_sw 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Player1 vers Waiting
force -freeze sim:/main/btn_push 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Waiting vers Player2
force -freeze sim:/main/btn_push 0 0
force -freeze sim:/main/etat_player 1 0
force -freeze sim:/main/etat_timer 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Player2 vers Waiting
force -freeze sim:/main/etat_timer 0 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run
force -freeze sim:/main/etat_player 0 0
force -freeze sim:/main/btn_push 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Waiting vers Player1
force -freeze sim:/main/etat_timer 0 0
force -freeze sim:/main/btn_push 0 0
force -freeze sim:/main/etat_timer 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Player1 vers Waiting avec timer out
force -freeze sim:/main/etat_timer 0 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run
force -freeze sim:/main/etat_timer 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Waiting vers Finished
force -freeze sim:/main/etat_timer 0 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run
force -freeze sim:/main/etat_timer 1 0
force -freeze sim:/main/is_finish 1 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run

#Etat Finished vers Off
force -freeze sim:/main/btn_sw 0 0
run
force -freeze sim:/main/clk 1 0
run
force -freeze sim:/main/clk 0 0
run