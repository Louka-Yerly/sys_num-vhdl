----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Haymoz Bryan
-- comment : Code for Main
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Main is
    Port (Btn_Sw : in  STD_LOGIC;
          Btn_Push : in  STD_LOGIC;
          Etat_Timer : in  STD_LOGIC;
          Is_Finish : in  STD_LOGIC;
          Etat_Player : in  STD_LOGIC;
          clk : in  STD_LOGIC;
          rst : in  STD_LOGIC;
          Etat_System : out  STD_LOGIC_VECTOR (2 downto 0));
end Main;

architecture Arch_Main of Main is

	TYPE etat IS (Off, Finished, Waiting, Player1, Player2);
	SIGNAL etat_present, etat_futur : etat;

begin

	registre : process(clk, rst)
	begin
		if rst = '1' then --reset
			etat_present <= Off;
		elsif rising_edge(clk) then -- clock flanc montant
			etat_present <= etat_futur;
		end if;
	end process registre;

	combi : process(etat_present, Btn_Sw, Btn_Push, Etat_Timer, Is_Finish, Etat_Player)
	begin
		case(etat_present) is
			when Off =>
				if Btn_Sw = '1' then
					etat_futur <= Player1;
				else
					etat_futur <= Off;
				end if;
			when Player1 =>
				if (Btn_Push = '1' OR Etat_Timer = '1') AND Btn_Sw = '1' then
					etat_futur <= Waiting;
				elsif Btn_Sw = '0' then
					etat_futur <= Off;
				else
					etat_futur <= Player1;
				end if;
			when Player2 =>
				if (Btn_Push = '1' OR Etat_Timer = '1') AND Btn_Sw = '1' then
					etat_futur <= Waiting;
				elsif Btn_Sw = '0' then
					etat_futur <= Off;
				else
					etat_futur <= Player2;
				end if;
			when Waiting =>
				if Is_Finish = '1' AND Btn_Sw = '1' then
					etat_futur <= Finished;
				elsif Is_Finish = '0' AND Etat_Player = '1' AND  Etat_Timer = '1' AND Btn_Sw = '1' then
					etat_futur <= Player1;
				elsif Is_Finish = '0' AND Etat_Player = '0' AND  Etat_Timer = '1' AND Btn_Sw = '1' then
					etat_futur <= Player2;
				elsif Btn_Sw = '0' then
          etat_futur <= Off;
        else
					etat_futur <= Waiting;
				end if;
			when Finished =>
				if Btn_Sw = '0' then
					etat_futur <= Off;
				else
					etat_futur <= Finished;
				end if;
			when others =>
					etat_futur <= Off;
		end case ;
	end process combi;

	with etat_present select
		Etat_System <= 	"000" when Off,
					   	      "001" when Player1,
						        "010" when Waiting,
						        "100" when Player2,
						        "111" when Finished;

end architecture ;
