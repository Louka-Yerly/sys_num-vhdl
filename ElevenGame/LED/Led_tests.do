restart

#Test etat Off
force -freeze sim:/led/clk 0 0
force -freeze sim:/led/rst 0 0
force -freeze sim:/led/etat_system 000 0
run
force -freeze sim:/led/clk 1 0
run
force -freeze sim:/led/clk 0 0
run

#Test etat Player1
force -freeze sim:/led/etat_system 001 0
run
force -freeze sim:/led/clk 1 0
run
force -freeze sim:/led/clk 0 0
run

#Test etat Waiting
force -freeze sim:/led/etat_system 010 0
run
force -freeze sim:/led/clk 1 0
run
force -freeze sim:/led/clk 0 0
run

#Test etat Player2
force -freeze sim:/led/etat_system 100 0
run
force -freeze sim:/led/clk 1 0
run
force -freeze sim:/led/clk 0 0
run

#Test etat Waiting
force -freeze sim:/led/etat_system 010 0
run
force -freeze sim:/led/clk 1 0
run
force -freeze sim:/led/clk 0 0
run

#Test etat Finished
force -freeze sim:/led/etat_system 111 0
run
force -freeze sim:/led/clk 1 0
run
force -freeze sim:/led/clk 0 0
run

#Test etat Off
force -freeze sim:/led/etat_system 000 0
run
force -freeze sim:/led/clk 1 0
run
force -freeze sim:/led/clk 0 0
run