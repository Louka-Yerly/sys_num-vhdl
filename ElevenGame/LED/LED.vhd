----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Haymoz Bryan
-- comment : Code for LED
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity LED is
    Port (Etat_System : in  STD_LOGIC_VECTOR (2 downto 0);
          clk : in  STD_LOGIC;
          rst : in  STD_LOGIC;
          Led_P1 : out  STD_LOGIC;
          Led_P2 : out  STD_LOGIC);
end LED;

architecture Arch_LED of LED is

  	SIGNAL etat_present_Led_P1, etat_futur_Led_P1, etat_present_Led_P2, etat_futur_Led_P2 : STD_LOGIC;

begin

  registre : process(clk, rst)
  begin
  if rst = '1' then
      etat_present_Led_P1 <= '0';
      etat_present_Led_P2 <= '0';
  elsif rising_edge(clk) then --reset
      etat_present_Led_P1 <= etat_futur_Led_P1;
      etat_present_Led_P2 <= etat_futur_Led_P2;
  end if;
  end process registre;

  combi : process(Etat_System)
  begin
    case(Etat_System) is
      when "001" => -- Player1
        etat_futur_Led_P1 <= '1';
        etat_futur_Led_P2 <= '0';
      when "100" => --Player2
        etat_futur_Led_P1 <= '0';
        etat_futur_Led_P2 <= '1';
      when "010" => --Waiting
        etat_futur_Led_P1 <= '0';
        etat_futur_Led_P2 <= '0';
      when "111" => --Finished
        etat_futur_Led_P1 <= '1';
        etat_futur_Led_P2 <= '1';
      when others => -- Others and Off
        etat_futur_Led_P1 <= '0';
        etat_futur_Led_P2 <= '0';
    end case;
  end process combi;

  Led_P1 <= etat_present_Led_P1;
  Led_P2 <= etat_present_Led_P2;

end Arch_LED;
