----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Haymoz Bryan
-- comment : Code for Current_Player
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Current_Player is
    Port (Etat_System : in  STD_LOGIC_VECTOR (2 downto 0);
          clk : in  STD_LOGIC;
          rst : in STD_LOGIC;
          Etat_Player : out  STD_LOGIC);
end Current_Player;

architecture Arch_Current_Player of Current_Player is

  	SIGNAL etat_present, etat_futur : STD_LOGIC;
    
begin

  registre : process(clk, rst)
  begin
  if rst = '1' then
      etat_present <= '0';
  elsif rising_edge(clk) then --reset
      etat_present <= etat_futur;
  end if;
  end process registre;

  combi : process(Etat_System, etat_present)
  begin
    case(Etat_System) is
      when "001" => -- Player1
        etat_futur <= '0';
      when "100" => --Player2
        etat_futur <= '1';
      when "010" => --Waiting
        etat_futur <= etat_present;
      when "111" => --Finished
        etat_futur <= etat_present;
      when others => -- Others and Off
        etat_futur <= '0';
    end case;
  end process combi;

  Etat_Player <= etat_present;

end Arch_Current_Player;
