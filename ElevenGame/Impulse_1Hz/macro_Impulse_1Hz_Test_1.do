restart
force -freeze sim:/impulse_1hz/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/impulse_1hz/rst 1 0
force -freeze sim:/impulse_1hz/etat_system 000 0
run 100 ns

force -freeze sim:/impulse_1hz/rst 0 0
run 100 ns

force -freeze sim:/impulse_1hz/etat_system 100 0
run 7000 ms


