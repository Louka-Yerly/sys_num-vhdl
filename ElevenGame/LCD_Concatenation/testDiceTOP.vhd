----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Yerly Louka
-- comment : 		Code to test the two Random
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity testDiceTOP is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           Etat_System : in  STD_LOGIC_VECTOR (2 downto 0);
           Dice1 : out  STD_LOGIC_VECTOR (2 downto 0);
           Dice2 : out  STD_LOGIC_VECTOR (2 downto 0));
end testDiceTOP;

architecture Arch_testDiceTOP of testDiceTOP is
	signal RDice1 : STD_LOGIC_VECTOR(2 downto 0);
	signal RDice2 : STD_LOGIC_VECTOR(2 downto 0);
begin
	
	D1 : entity work.Random_Dice1(Arch_Random_Dice1)
		port map (
			clk => clk, rst => rst, Etat_System => Etat_System, Random_1 => RDice1
		);

	D2 : entity work.Random_Dice2(Arch_Random_Dice2)
		port map (
			clk => clk, rst => rst, Etat_System => Etat_System, Random_2 => RDice2
		);

	Dice1 <= RDice1;
	Dice2 <= RDice2;

end Arch_testDiceTOP;

