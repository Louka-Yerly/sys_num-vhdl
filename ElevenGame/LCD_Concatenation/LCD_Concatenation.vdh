----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Yerly Louka
-- comment : 		Code for the dialog with the LCD
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library work;
use work.arr8bit_PKG.all;

entity LCD_Concatenation is
    Port ( clk 			: in 	STD_LOGIC;
           rst 			: in 	STD_LOGIC;
           Etat_System 	: in 	STD_LOGIC_VECTOR (2 downto 0);
           Etat_Player	: in 	STD_LOGIC;
           Str_Top		: in 	arr8bit(0 to 5);
           Str_Bottom 	: in 	arr8bit(0 to 4);
           LCD_RS 		: out 	STD_LOGIC;
           LCD_RW 		: out 	STD_LOGIC;
           LCD_E 		: out 	STD_LOGIC;
           --LCD_Data 	: out 	arr8bit(0 to 31)); -- 0 to 31 for the test but 0 to 8 when it will be implemented
           LCD_Data 	: out 	STD_LOGIC_VECTOR(7 downto 0));

end LCD_Concatenation;

architecture Arch_LCD_Concatenation of LCD_Concatenation is
-- Signal
	signal LCD_present, LCD_futur : arr8bit(0 to 31);

-- Constant
	constant data_Winner_Top 	 : arr8bit(0 to 15) :=	(x"20",x"54",x"68",x"65",x"20",x"77",x"69",x"6E",x"6E",
                                              			 x"65",x"72",x"20",x"69",x"73",x"20",x"20");
	constant data_Winner_Bottom	 : arr8bit(0 to 15) := 	(x"20",x"20",x"20",x"20",x"50",x"6C",x"61",x"79",x"65",
  													 	 x"72",x"20",x"31",x"20",x"20",x"20",x"20");
	constant data_Dynamic_Top	 : arr8bit(0 to 15) :=  (x"50",x"31",x"3A",x"20",x"20",x"20",x"20",x"20",x"20",
                                                		 x"20",x"20",x"50",x"32",x"3A",x"20",x"20");
	constant data_Dynamic_Bottom : arr8bit(0 to 15) := 	(x"20",x"20",x"20",x"20",x"2F",x"20",x"20",x"3d",x"3e",
														 x"20",x"20",x"20",x"20",x"20",x"20",x"20");
	constant data_Nothing		 : arr8bit(0 to 15) :=  (x"20",x"20",x"20",x"20",x"20",x"20",x"20",x"20",x"20",
                                                		 x"20",x"20",x"20",x"20",x"20",x"20",x"20");

begin
	registre : process(rst, clk)
	begin
		if rst = '1' then
			LCD_present <= (others => (others => '0'));
		elsif rising_edge(clk) then
			LCD_present <= LCD_futur;
		end if;
	end process ; -- registre

	combi : process(Etat_System, LCD_present, Str_Top, Str_Bottom, Etat_Player)
		variable array_temp : arr8bit(31 downto 0);

	begin
		if Etat_System = "111" then -- Finished
			array_temp(31 downto 16)  := data_Winner_Top;
			array_temp(15 downto 0) := data_Winner_Bottom;
			if Etat_Player = '0' then
				-- Player2 win
				array_temp(4) := x"32";
			else
				-- Player1 win
			 	array_temp(4) := x"31";
			end if ;

			LCD_futur <= array_temp;

		elsif Etat_System = "000" then -- Off
			array_temp(31 downto 16) := data_Dynamic_Top;
			array_temp(15 downto 0)  := data_Nothing;
			array_temp(28) := Str_Top(0);
			array_temp(27) := Str_Top(1);
			array_temp(24) := Str_Top(2);
			array_temp(23) := Str_Top(3);
			array_temp(17) := Str_Top(4);
			array_temp(16) := Str_Top(5);

			LCD_futur <= array_temp;

		elsif Etat_System = "010" then -- Waiting
			array_temp(31 downto 16) := data_Dynamic_Top;
			array_temp(15 downto 0)  := data_Dynamic_Bottom;
			array_temp(28) := Str_Top(0);
			array_temp(27) := Str_Top(1);
			array_temp(24) := Str_Top(2);
			array_temp(23) := Str_Top(3);
			array_temp(17) := Str_Top(4);
			array_temp(16) := Str_Top(5);

			array_temp(12) := Str_Bottom(0);
			array_temp(10) := Str_Bottom(1);
			array_temp(5)  := Str_Bottom(2);
			array_temp(4)  := Str_Bottom(3);
			array_temp(3)  := Str_Bottom(4);

			LCD_futur <= array_temp;

		else -- Player1 and Player2 (and other Parasistic state)
			array_temp := LCD_present;
			LCD_futur <= array_temp;

		end if ;
	end process ; -- combi

	LCD_RS <= '1';
	LCD_RW <= '1';
	LCD_E  <= '1';
	LCD_Data <= "00000000";--LCD_present;
end Arch_LCD_Concatenation;
