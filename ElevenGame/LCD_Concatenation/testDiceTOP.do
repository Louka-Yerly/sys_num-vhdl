#=========================================
#				Initialisation
#=========================================
force -freeze sim:/testDiceTOP/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/testDiceTOP/rst 1 0
force -freeze sim:/testDiceTOP/Etat_System 000 0
run 100 ns
force -freeze sim:/testDiceTOP/rst 0 0
run 100 ns

#=========================================
#		Test Dice: Etat Off
#=========================================
force -freeze sim:/testDiceTOP/Etat_System 000 0
force -freeze sim:/testDiceTOP/clk 1 0, 0 {7812500 ps} -r {15625 ns}
run 2 ms
 
#=========================================
#		Test Dice: Etat Player1
#=========================================
force -freeze sim:/testDiceTOP/Etat_System 001 0
run 2 ms