restart
force -freeze sim:/points_calculation/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/points_calculation/rst 1 0
run 15625 ns

force -freeze sim:/points_calculation/rst 0 0

force -freeze sim:/points_calculation/etat_system 111 0
force -freeze sim:/points_calculation/etat_timer 0 0
force -freeze sim:/points_calculation/etat_player 0 0
force -freeze sim:/points_calculation/random_1 001 0
force -freeze sim:/points_calculation/random_2 100 0
force -freeze sim:/points_calculation/currentcagnotte_p1 11111100 0
force -freeze sim:/points_calculation/currentcagnotte_p2 00100110 0
force -freeze sim:/points_calculation/currentjackpot 0101110 0

run 46875 ns

force -freeze sim:/points_calculation/etat_system 000 0

run 15625 ns

force -freeze sim:/points_calculation/currentcagnotte_p1 00101000 0
force -freeze sim:/points_calculation/currentcagnotte_p2 00101000 0
force -freeze sim:/points_calculation/currentjackpot 0000000 0
run 46875 ns