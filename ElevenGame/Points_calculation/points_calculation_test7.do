restart
force -freeze sim:/points_calculation/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/points_calculation/rst 1 0
run 15625 ns

force -freeze sim:/points_calculation/rst 0 0

force -freeze sim:/points_calculation/etat_system 010 0
force -freeze sim:/points_calculation/etat_timer 0 0
force -freeze sim:/points_calculation/etat_player 0 0
force -freeze sim:/points_calculation/random_1 001 0
force -freeze sim:/points_calculation/random_2 001 0
force -freeze sim:/points_calculation/currentcagnotte_p1 00001001 0
force -freeze sim:/points_calculation/currentcagnotte_p2 00101001 0
force -freeze sim:/points_calculation/currentjackpot 0011110 0

run 200 us