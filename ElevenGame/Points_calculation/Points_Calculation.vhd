----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Vial Maël
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Points_Calculation is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           Etat_system : in  STD_LOGIC_VECTOR (2 downto 0);
           Etat_timer : in  STD_LOGIC;
           Etat_player : in STD_LOGIC;
           Random_1 : in  STD_LOGIC_VECTOR  (2 downto 0);
           Random_2 : in  STD_LOGIC_VECTOR  (2 downto 0);
           CurrentCagnotte_P1 : in  STD_LOGIC_VECTOR (7 downto 0);
           CurrentCagnotte_P2 : in  STD_LOGIC_VECTOR (7 downto 0);
           CurrentJackpot : in  STD_LOGIC_VECTOR (6 downto 0);
           Is_Finish : out  STD_LOGIC;
           Dice_1 : out  STD_LOGIC_VECTOR  (2 downto 0);
           Dice_2 : out  STD_LOGIC_VECTOR  (2 downto 0);
           Dice_effect : out STD_LOGIC_VECTOR (7 downto 0);
           NextCagnotte_P1 : out  STD_LOGIC_VECTOR (7 downto 0);
           NextCagnotte_P2 : out  STD_LOGIC_VECTOR (7 downto 0);
           NextJackpot : out  STD_LOGIC_VECTOR (6 downto 0));
end Points_Calculation;

architecture Arch_Points_Calculation of Points_Calculation is
  -- Signal
	signal dice1_present, dice1_futur : UNSIGNED (2 downto 0);
  signal dice2_present, dice2_futur : UNSIGNED (2 downto 0);
  signal dice_effect_present_p1, dice_effect_futur_p1 : SIGNED (7 downto 0);
  signal dice_effect_present_p2, dice_effect_futur_p2 : SIGNED (7 downto 0);
  signal cagnotte_futur_p1, cagnotte_futur_p2 : SIGNED (7 downto 0);
  signal jackpot_futur : UNSIGNED (6 downto 0);
  signal timer_tour_present, timer_tour_futur : STD_LOGIC;
  signal is_finished_futur : STD_LOGIC;
  signal saved : std_logic;
  signal compteur : unsigned(2 downto 0);

  constant compteur_max : unsigned := "010";

begin
  	registre : process(clk, rst)
  	begin
      -- reset the registre values
  		if rst = '1' then
  			dice1_present <= "000";
        dice2_present <= "000";
        dice_effect_present_p1 <= "00000000";
        dice_effect_present_p2 <= "00000000";
        timer_tour_present <= '0';
  		elsif rising_edge(clk) then
  			dice1_present <= dice1_futur;
        dice2_present <= dice2_futur;
        dice_effect_present_p1 <= dice_effect_futur_p1;
        dice_effect_present_p2 <= dice_effect_futur_p2;
        timer_tour_present <= timer_tour_futur;
  		end if;
  	end process ;

  	combi : process(Etat_System, Random_1, Random_2)
  	begin
  		case Etat_System is
  			when "001" => -- Player1, set dice to 0, no calculation will take place
        dice1_futur <= "000";
        dice2_futur <= "000";

  			when "100" => -- Player2, set dice to 0, no calculation will take place
        dice1_futur <= "000";
        dice2_futur <= "000";

  			when "010" => -- Waiting, get the random values, calculations will take place
        dice1_futur <= UNSIGNED(Random_1);
        dice2_futur <= UNSIGNED(Random_2);

  			when "111" => -- Finished, set dice to 0, no calculation will take place
  				dice1_futur <= "000";
          dice2_futur <= "000";

  			when others => -- Off and all other parasitic state,, set dice to 0, no calculation will take place
  				dice1_futur <= "000";
          dice2_futur <= "000";

  		end case;
  	end process ;


    calc_dice_effect : process(dice1_present, dice2_present, Etat_player, CurrentJackpot, timer_tour_present, Etat_System)
    variable dice_addition : UNSIGNED (3 downto 0) := ('0' & dice1_futur) + ('0' & dice2_futur);
    begin
      dice_effect_futur_p1 <= "00000000";
      dice_effect_futur_p2 <= "00000000";
      dice_addition := ('0' & dice1_present) + ('0' & dice2_present);

      case( Etat_player ) is

        -- Player 1 just played
        when '0' =>

          -- dice_addition = 0, it means Etat_System != Waiting
          if dice_addition = "0000" then
              dice_effect_futur_p1 <= "00000000";

          -- player1 is to long to play
          elsif timer_tour_present = '1' then
            dice_effect_futur_p1 <= to_signed(-15, 8);

          -- dice_addition = 12
          elsif dice_addition = "1100" then
            dice_effect_futur_p1 <= to_signed(-12, 8);

            -- dice_addition = 11
            elsif dice_addition = "1011" then
              dice_effect_futur_p1 <= SIGNED('0' & CurrentJackpot);

            -- dice_addition = 1-10
            else
              dice_effect_futur_p1 <= to_signed(-11, 8) + SIGNED("0000" & dice_addition);
          end if;

        -- Player 1 just played
        when '1' =>

            -- dice_addition = 0, it means Etat_System != Waiting
            if dice_addition = "0000" then
                dice_effect_futur_p2 <= "00000000";

            -- player2 is to long to play
            elsif timer_tour_present = '1' then
              dice_effect_futur_p2 <= to_signed(-15, 8);

            -- dice_addition = 12
            elsif dice_addition = "1100" then
              dice_effect_futur_p2 <= to_signed(-12, 8);

            -- dice_addition = 11
            elsif dice_addition = "1011" then
              dice_effect_futur_p2 <= SIGNED('0' & CurrentJackpot);

            -- dice_addition = 1-10
            else
              dice_effect_futur_p2 <= to_signed(-11, 8) + SIGNED("0000" & dice_addition);
          end if;

        when others => -- Paristic case
        dice_effect_futur_p1 <= "00000000";
        dice_effect_futur_p2 <= "00000000";
      end case;
    end process;

    compute_etat_timer: process(Etat_timer, Etat_system, timer_tour_present)
    begin
      --Player didn't play in time
      if Etat_timer = '1' then
        timer_tour_futur <= '1';

      --We want to reset the value
      elsif Etat_system = "001" or Etat_system = "100" or Etat_system = "000" or Etat_system = "111" then
        timer_tour_futur <= '0';

      --We want to keep the actual value
      elsif Etat_system = "010" then
          timer_tour_futur <= timer_tour_present;

      --Paristic case
      else
          timer_tour_futur <= '0';
      end if;
    end process;

    next_jackpot: process(dice_effect_present_p1, dice_effect_present_p2, CurrentJackpot, Etat_System)
    variable jackpot_temp : SIGNED(7 downto 0) := "00000000";
    begin
      -- We want to compute the new Jackpot
      if Etat_System = "010" and saved = '0' then
        jackpot_temp := "00000000";
        jackpot_temp := SIGNED('0' & CurrentJackpot) + ("00000000" - dice_effect_present_p1 - dice_effect_present_p2);
        jackpot_futur <= UNSIGNED(jackpot_temp(6 downto 0));

      -- Just do nothing
      else
        jackpot_futur <= UNSIGNED(CurrentJackpot);
      end if;
    end process;

    next_cagnotte: process(dice_effect_present_p1, dice_effect_present_p2, CurrentCagnotte_P1, CurrentCagnotte_P2, Etat_System)
    begin
      -- We want to compute the new cagnotte
      if Etat_System = "010" and saved = '0' then
        cagnotte_futur_p1 <= SIGNED(CurrentCagnotte_P1) + dice_effect_present_p1;
        cagnotte_futur_p2 <= SIGNED(CurrentCagnotte_P2) + dice_effect_present_p2;

      -- Just do nothing
      else
        cagnotte_futur_p1 <= SIGNED(CurrentCagnotte_P1);
        cagnotte_futur_p2 <= SIGNED(CurrentCagnotte_P2);
      end if;
    end process;

    is_loosed: process(cagnotte_futur_p1, cagnotte_futur_p2)
    begin

      -- Player1 lost
      if cagnotte_futur_p1 < "00000000" then
        is_finished_futur <= '1';

      -- Player2 lost
      elsif cagnotte_futur_p2 < "00000000" then
        is_finished_futur <= '1';

      -- No one lost
      else
        is_finished_futur <= '0';
      end if;
    end process;

  	Dice_1 <= STD_LOGIC_VECTOR(dice1_present);
    Dice_2 <= STD_LOGIC_VECTOR(dice2_present);
    Dice_effect <= STD_LOGIC_VECTOR(dice_effect_present_p1 + dice_effect_present_p2);

    -- Initialze jackpot to 0 and cagnotte to 40 when Etat_system is Off for the next game
    NextJackpot <= "0000000" when Etat_system = "000" else STD_LOGIC_VECTOR(jackpot_futur);
    NextCagnotte_P1 <= "00101000" when Etat_system = "000" else STD_LOGIC_VECTOR(cagnotte_futur_p1);
    NextCagnotte_P2 <= "00101000" when Etat_system = "000" else STD_LOGIC_VECTOR(cagnotte_futur_p2);
    Is_Finish <= is_finished_futur;

    saved <= '1' when compteur = compteur_max else '0';

    sauver : process(clk, rst)
    begin
      if rst = '1' then
          compteur <= (others => '0');
      elsif rising_edge(clk) then -- clock flanc montant
          if Etat_System = "010" then
            if compteur < compteur_max then
                compteur <= compteur + 1;
            end if;
          else
            compteur <= (others => '0');
          end if;
      end if;
    end process sauver;

end Arch_Points_Calculation;
