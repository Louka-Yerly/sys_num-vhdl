----------------------------------------------------------------------------------
-- Group number:	5 
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Yerly Louka
-- comment : 		A comment example
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity EntityName is
    Port ( clk 			: in  	STD_LOGIC;
           rst 			: in 	STD_LOGIC;
           Etat_System 	: in 	STD_LOGIC_VECTOR(2 downto 0);
           MyInput 		: in  	STD_LOGIC;
           MyOutput 	: out 	STD_LOGIC_VECTOR (3 downto 0));
end EntityName;

architecture Arch_EntityName of EntityName is
	-- Signal
	signal valeur_presente, valeur_furture : STD_LOGIC_VECTOR(3 downto 0);

	-- Constant
	constant init_value : STD_LOGIC_VECTOR(3 downto 0) := ("0000");
begin

	registre : process(clk, rst)
	begin
		if rst = '1' then
			valeur_presente <= init_value;
		elsif rising_edge(clk) then
			valeur_presente <= valeur_furture;
		end if;		
	end process ; -- registre

	combi : process(Etat_System, MyInput)
	begin
		case Etat_System is
			when "001" => -- Player1
				--Do something

			when "100" => -- Player2
				--Do something

			when "010" => -- Waiting
				-- Do something

			when "111" => -- Finished
				-- Do something in example this
				-- you want just do nothing
				valeur_furture <= valeur_presente;

			when others => -- Off and all other parasitic state
				-- Do something, in example reset
				-- to initial value
				valeur_furture <= init_value;

		end case;
	end process ; -- combi

	MyOutput <= valeur_presente;

end Arch_EntityName;
