#=========================================
#				Initialisation
#=========================================
force -freeze sim:/Random_Dice2/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/Random_Dice2/rst 1 0
force -freeze sim:/Random_Dice2/Etat_System 000 0
run 100 ns
force -freeze sim:/Random_Dice2/rst 0 0
run 100 ns

#=========================================
#		Test Dice: Etat Off
#=========================================
force -freeze sim:/Random_Dice2/Etat_System 000 0
run 2 ms
 
#=========================================
#		Test Dice: Etat Player1
#=========================================
force -freeze sim:/Random_Dice2/Etat_System 001 0
run 2 ms (pour 2 changement c'est 13 coup de clock)

#=========================================
#		Test Dice: Etat Waiting
#=========================================
force -freeze sim:/Random_Dice2/Etat_System 010 0
run 2 ms

#=========================================
#		Test Dice: Etat Player2
#=========================================
force -freeze sim:/Random_Dice2/Etat_System 100 0
run 2 ms

#=========================================
#		Test Dice: Etat Finished
#=========================================
force -freeze sim:/Random_Dice2/Etat_System 111 0
run 2 ms

#=========================================
#		Test Dice: rst
#=========================================
force -freeze sim:/Random_Dice2/rst 1 0
run 2 ms