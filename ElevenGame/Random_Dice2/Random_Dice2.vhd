----------------------------------------------------------------------------------
-- Group number:	5
-- Group member:	Haymoz Bryan, Vial Maël, Schroeter Maxime, Yerly Louka
-- Editor's name:	Yerly Louka
-- comment : 		Code for Random_2
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Random_Dice2 is
    Port ( clk         : in  STD_LOGIC;
           rst         : in  STD_LOGIC;
           Etat_System : in  STD_LOGIC_VECTOR(2 downto 0);
           Random_2    : out STD_LOGIC_VECTOR(2 downto 0));
end Random_Dice2;

architecture Arch_Random_Dice2 of Random_Dice2 is
-- Signal
	signal valeur_presente, valeur_furture 	: unsigned(2 downto 0);
	signal compteur_present, compteur_futur : unsigned(4 downto 0);
	signal compte, fin_comptage             : STD_LOGIC;

-- Constant
	constant valeur_min   : unsigned(2 downto 0) := to_unsigned(1, 3);
	constant valeur_max   : unsigned(2 downto 0) := to_unsigned(6, 3);
	constant compteur_max : unsigned(4 downto 0) := to_unsigned(17, 5);

begin

	fin_comptage <= '1' when compteur_present = compteur_max and (Etat_System = "001" or Etat_System = "100")
						else '0';

	compte <= '1' when Etat_System = "100" or Etat_System = "001"
				  else '0';

	registre : process(clk, rst)
	begin
		if rst = '1' then
			valeur_presente <= valeur_max;
		elsif rising_edge(clk) then
			valeur_presente <= valeur_furture;
		end if;
	end process ; -- registre

	combi_valeur : process(valeur_presente, fin_comptage)
	begin
		if fin_comptage = '1' then
			if valeur_presente-1<valeur_min then
				valeur_furture <= valeur_max;
			else
				valeur_furture <= valeur_presente - 1;
			end if;
		else
			valeur_furture <= valeur_presente;
		end if ;
	end process ; -- combi_valeur

	registre_compteur : process(clk, rst)
	begin
		if rst = '1' then
			compteur_present <= (others => '0');
		elsif rising_edge(clk) then
			compteur_present <= compteur_futur;
		end if;
	end process ; -- registre_compteur

	combi_compteur : process(compteur_present, compte, fin_comptage)
	begin
		if compte = '0' or fin_comptage ='1' then
			compteur_futur <= (others => '0');
		else
			compteur_futur <= compteur_present + 1;
		end if;
	end process ; -- combi_compteur

	Random_2 <= STD_LOGIC_VECTOR(valeur_presente);

end Arch_Random_Dice2;

