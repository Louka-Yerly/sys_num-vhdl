
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


package pkg_constantes_functions is
    -- Constants
    constant dig_c      : std_logic_vector(3 downto 0) := "1010";
    constant dig_t      : std_logic_vector(3 downto 0) := "1011";
    constant dig_h      : std_logic_vector(3 downto 0) := "1100";
    constant dig_e      : std_logic_vector(3 downto 0) := "1101";
    constant dig_q      : std_logic_vector(3 downto 0) := "1110";
    constant dig_eteint : std_logic_vector(3 downto 0) := "1111";

    constant addr_length : positive := 16;
    constant key_length  : positive := 16;

    constant key_CHm  : std_logic_vector((key_length/2)-1 downto 0) := x"5D"; -- CH-
    constant key_CH   : std_logic_vector((key_length/2)-1 downto 0) := x"9D"; -- CH
    constant key_CHp  : std_logic_vector((key_length/2)-1 downto 0) := x"1D"; -- CH+
    constant key_PREV : std_logic_vector((key_length/2)-1 downto 0) := x"DD"; -- Prev
    constant key_NEXT : std_logic_vector((key_length/2)-1 downto 0) := x"FD"; -- Next
    constant key_PLAY : std_logic_vector((key_length/2)-1 downto 0) := x"3D"; -- Play/Pause
    constant key_VOLm : std_logic_vector((key_length/2)-1 downto 0) := x"1F"; -- Vol-
    constant key_VOLp : std_logic_vector((key_length/2)-1 downto 0) := x"57"; -- Vol+
    constant key_EQ   : std_logic_vector((key_length/2)-1 downto 0) := x"6F"; -- EQ
    constant key_0    : std_logic_vector((key_length/2)-1 downto 0) := x"97"; -- 0
    constant key_100p : std_logic_vector((key_length/2)-1 downto 0) := x"67"; -- 100+
    constant key_200p : std_logic_vector((key_length/2)-1 downto 0) := x"4F"; -- 200+
    constant key_1    : std_logic_vector((key_length/2)-1 downto 0) := x"CF"; -- 1
    constant key_2    : std_logic_vector((key_length/2)-1 downto 0) := x"E7"; -- 2
    constant key_3    : std_logic_vector((key_length/2)-1 downto 0) := x"85"; -- 3
    constant key_4    : std_logic_vector((key_length/2)-1 downto 0) := x"EF"; -- 4
    constant key_5    : std_logic_vector((key_length/2)-1 downto 0) := x"C7"; -- 5
    constant key_6    : std_logic_vector((key_length/2)-1 downto 0) := x"A5"; -- 6
    constant key_7    : std_logic_vector((key_length/2)-1 downto 0) := x"BD"; -- 7
    constant key_8    : std_logic_vector((key_length/2)-1 downto 0) := x"B5"; -- 8
    constant key_9    : std_logic_vector((key_length/2)-1 downto 0) := x"AD"; -- 9

                                      ------------------
                                --  /                    \
                                -- |  CH-    CH     CH+   |
                                -- | FFA25D FF629D FFE21D |
                                -- |                      |
                                -- |  |<<     >>|   |>||  |
                                -- | FF22DD FF02FD FFC23D |
                                -- |                      |
                                -- |   -       +     EQ   |
                                -- | FFE01F FFA857 FF906F |
                                -- |                      |
                                -- |   0      100+  200+  |
                                -- | FF6897 FF9867 FFB04F |
                                -- |                      |
                                -- |   1       2     3    |
                                -- | FF30CF FF18E7 FF7A85 |
                                -- |                      |
                                -- |   4       5     6    |
                                -- | FF10EF FF38C7 FF5AA5 |
                                -- |                      |
                                -- |   7       8     9    |
                                -- | FF42BD FF4AB5 FF52AD |
                                -- |                      |
                                -- |         Car          |
                                -- |         mp3          |
                                --  \                    /
                                      ------------------

    -- Functions
    function ntob(number: natural) return natural;

end pkg_constantes_functions;

package body pkg_constantes_functions is

    function ntob(number: natural) return natural is
        variable res_v : natural; -- Result
        variable remain_v : natural; -- Remainder used in iteration
    begin
        res_v := 0;
        remain_v := number;
        while remain_v > 0 loop -- Iteration for each bit required
            res_v := res_v + 1;
            remain_v := remain_v / 2;
        end loop;
        return res_v;
    end function ntob;
    
end pkg_constantes_functions;