library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.pkg_constantes_functions.all;
use IEEE.NUMERIC_STD.ALL;


entity main is
    Generic ( freq_horloge : positive := 50_000_000);
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           key_pressed, address_remote : in STD_LOGIC_VECTOR ((key_length/2)-1 downto 0);
           key_holded, valid_data, done : in STD_LOGIC;

           dig1, dig2, dig3, dig4 : out STD_LOGIC_VECTOR (3 downto 0);
           dot1, dot2, dot3, dot4 : out STD_LOGIC);
end main;

architecture Behavioral of main is
    type ETATS is (standby, stockage, affiche_key, affiche_key_holded, affiche_error);

    -- Constant
    constant blink : positive := freq_horloge/4;

    -- State machine
    signal etat_present, etat_futur : ETATS;

    -- Gestion I/O
    signal sig_dig1, sig_dig2, sig_dig3, sig_dig4 : STD_LOGIC_VECTOR(dig1'range);
    signal sig_dot4 : STD_LOGIC;

    signal sig_key_pressed, sig_address_remote : STD_LOGIC_VECTOR(key_pressed'range);

    -- Gestion du clinotage
    signal start_blink : STD_LOGIC;
    signal sig_blink_value : STD_LOGIC_VECTOR(ntob(blink) downto 0);
    signal fin_blink : STD_LOGIC;
     
begin
    
    dig1 <= sig_dig1 when etat_present = affiche_key or etat_present = affiche_key_holded else dig_eteint;
    dig2 <= sig_dig2 when etat_present = affiche_key or etat_present = affiche_key_holded else dig_eteint;
    dig3 <= sig_dig3 when etat_present = affiche_key or etat_present = affiche_key_holded else dig_eteint;
    dig4 <= sig_dig4 when etat_present = affiche_key or etat_present = affiche_key_holded else dig_eteint;
    dot1 <= '0';
    dot2 <= '0';
    dot3 <= '1' when etat_present = affiche_error else '0';
    dot4 <= sig_dot4;
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    fin_blink <= '1' when unsigned(sig_blink_value) >= to_unsigned(blink, sig_blink_value'length) else '0';
    start_blink <= '1' when etat_present = affiche_key_holded and fin_blink = '0' else '0';
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    compteur_dot : entity work.compteur_generic(Behavioral)
        port map(clk => clk, rst => rst, startC => start_blink, incr => '1', value => sig_blink_value);
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    registre : process(clk, rst)
    begin
        if rst = '1' then
            etat_present <= standby;
        elsif rising_edge(clk) then
            etat_present <= etat_futur;
        end if ;
    end process ; -- registre

    state_machine : process(etat_present, done, valid_data, key_holded)
    begin
        case etat_present is
        
            when standby =>
                if done = '1' then
                    etat_futur <= stockage;
                else
                    etat_futur <= standby;
                end if ;
            when stockage =>
                if valid_data = '1' then
                    etat_futur <= affiche_key;
                else
                    etat_futur <= affiche_error;
                end if ;
            when affiche_key =>
                if done = '1' then
                    etat_futur <= stockage;
                elsif key_holded = '1' then
                    etat_futur <= affiche_key_holded;
                else
                    etat_futur <= affiche_key;
                end if ;
            when affiche_key_holded =>
                if key_holded = '0' then
                    etat_futur <= affiche_key;
                else
                    etat_futur <= affiche_key_holded;
                end if ;
            when affiche_error =>
                if done = '1' then
                    etat_futur <= stockage;
                else
                    etat_futur <= affiche_error;
                end if ;
            when others =>
                etat_futur <= standby;     
        end case ;
    end process ; -- state_machine
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    
    registre_sig_key_pressed : process(clk, rst)
    begin
        if rst = '1' then
            sig_key_pressed <= (others => '0');
        elsif rising_edge(clk) then
            if etat_present = stockage then
                sig_key_pressed <= key_pressed;
            end if ;
        end if ;
    end process ; -- registre_sig_key_pressed

    registre_sig_address_remote : process(clk, rst)
    begin
        if rst = '1' then
            sig_address_remote <= (others => '0');
        elsif rising_edge(clk) then
            if etat_present = stockage then
                sig_address_remote <= address_remote;
            end if ;
        end if ;
    end process ; -- registre_sig_address_remote
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    combi_sig_key_pressed : process(sig_key_pressed)
    begin
        case sig_key_pressed is
        
            when key_CHm =>
                sig_dig1 <= dig_c;
                sig_dig2 <= dig_h;
                sig_dig3 <= x"1";
                sig_dig4 <= dig_eteint;
            when key_CH =>
                sig_dig1 <= dig_c;
                sig_dig2 <= dig_h;
                sig_dig3 <= x"2";
                sig_dig4 <= dig_eteint;
            when key_CHp =>
                sig_dig1 <= dig_c;
                sig_dig2 <= dig_h;
                sig_dig3 <= x"3";
                sig_dig4 <= dig_eteint;
            when key_PREV =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= dig_eteint;
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_NEXT =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= dig_eteint;
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_PLAY =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= dig_eteint;
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_VOLm =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= dig_eteint;
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_VOLp =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= dig_eteint;
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_EQ =>
                sig_dig1 <= dig_e;
                sig_dig2 <= dig_q;
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_0 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"0";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_100p =>
                sig_dig1 <= x"1";
                sig_dig2 <= x"0";
                sig_dig3 <= x"0";
                sig_dig4 <= dig_eteint;
            when key_200p =>
                sig_dig1 <= x"2";
                sig_dig2 <= x"0";
                sig_dig3 <= x"0";
                sig_dig4 <= dig_eteint;
            when key_1 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"1";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_2 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"2";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_3 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"3";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_4 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"4";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_5 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"5";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_6 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"6";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_7 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"7";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_8 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"8";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
            when key_9 =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= x"9";
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;        
            when others =>
                sig_dig1 <= dig_eteint;
                sig_dig2 <= dig_eteint;
                sig_dig3 <= dig_eteint;
                sig_dig4 <= dig_eteint;
        
        end case ;
    end process ; -- combi_dig1
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    registre_sig_dot4:process(clk, rst)
    begin
        if rst = '1' then
            sig_dot4 <= '0';
        elsif rising_edge(clk) then
            if etat_present = affiche_key then
                sig_dot4 <= '1';
            elsif etat_present = affiche_key_holded and fin_blink = '1' then
                sig_dot4 <= not sig_dot4;
            elsif etat_present = affiche_error or etat_present = standby then
                sig_dot4 <= '0';
            end if;
        end if;
    end process; -- registre_dot4
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
end Behavioral;
