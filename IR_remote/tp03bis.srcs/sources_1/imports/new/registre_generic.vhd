library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity registre_generic is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           store : in STD_LOGIC;
           valeur_future : in STD_LOGIC_VECTOR;
           valeur_presente : out STD_LOGIC_VECTOR);
           
end registre_generic;

architecture Behavioral of registre_generic is
begin
	
	registre : process(clk, rst)
	begin
		if rst = '1' then
			valeur_presente <= (others => '0');
		elsif rising_edge(clk) then
			if store = '1' then
				valeur_presente <= valeur_future;	
			end if ;
		end if ;
	end process ; -- registre

end Behavioral;
