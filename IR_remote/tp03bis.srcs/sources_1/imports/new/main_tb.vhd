library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.pkg_constantes_functions.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main_tb is
end main_tb;

architecture Behavioral of main_tb is

    signal clk_tb : STD_LOGIC := '0';
    signal clk_period : time := 20 ns;
    signal rst_tb : STD_LOGIC := '0';

    signal fin_testbench : boolean := false;

    signal error_pulse : std_logic;
    constant error_pulse_duration : time := 1 ns;

    signal key_pressed_sti : STD_LOGIC_VECTOR((key_length/2)-1 downto 0) := x"00";
    signal address_remote_sti : STD_LOGIC_VECTOR((addr_length/2)-1 downto 0) := x"00";
    signal key_holded_sti : STD_LOGIC := '0';
    signal valid_data_sti : STD_LOGIC := '0';
    signal done_sti : STD_LOGIC := '0';
    signal dig1_obs, dig2_obs, dig3_obs, dig4_obs : STD_LOGIC_VECTOR (3 downto 0);
    signal dot1_obs, dot2_obs, dot3_obs, dot4_obs : STD_LOGIC;

    procedure receive_data(signal addr : out STD_LOGIC_VECTOR(address_remote_sti'range);
                                 signal key : out STD_LOGIC_VECTOR(key_pressed_sti'range);
                                 signal done: out STD_LOGIC;
                                 signal valid_data : out STD_LOGIC;
                                 constant addr_in : in STD_LOGIC_VECTOR(address_remote_sti'range);
                                 constant key_in : in STD_LOGIC_VECTOR(key_pressed_sti'range);
                                 constant valid_in : in STD_LOGIC) is
    begin
        addr <= addr_in;
        key <= key_in;
        wait until rising_edge(clk_tb);
        valid_data <= valid_in;
        done <= '1';
        wait until rising_edge(clk_tb);
        done <= '0';
    end receive_data;

    procedure receive_repeat(signal addr : out STD_LOGIC_VECTOR(address_remote_sti'range);
                             signal key : out STD_LOGIC_VECTOR(key_pressed_sti'range);
                             signal done: out STD_LOGIC;
                             signal valid_data : out STD_LOGIC;
                             signal hold : out STD_LOGIC;
                             constant addr_in : in STD_LOGIC_VECTOR(address_remote_sti'range);
                             constant key_in : in STD_LOGIC_VECTOR(key_pressed_sti'range);
                             constant valid_in : in STD_LOGIC) is
    begin

        receive_data(addr, key, done, valid_data, addr_in, key_in, valid_in);
        wait for 40.5 ms;
        hold <= '1';
        wait for 1 sec;
        hold <= '0';        
    end receive_repeat;

begin
    
    UUT : entity work.main(Behavioral)
            port map(clk => clk_tb, rst => rst_tb, key_pressed => key_pressed_sti,
                    address_remote => address_remote_sti, key_holded => key_holded_sti,
                    valid_data => valid_data_sti, done => done_sti,
                    dig1 => dig1_obs, dig2 => dig2_obs, dig3 => dig3_obs, dig4 => dig4_obs,
                    dot1 => dot1_obs, dot2 => dot2_obs, dot3 => dot3_obs, dot4 => dot4_obs);
    
    reset_process : process
    begin
        rst_tb <= '1';
        wait for clk_period;
        assert dig1_obs = dig_eteint
            report "Erreur valeur de sortie au reset" severity failure;
        assert dig2_obs = dig_eteint
            report "Erreur valeur de sortie au reset" severity failure;
        assert dig3_obs = dig_eteint
            report "Erreur valeur de sortie au reset" severity failure;
        assert dig4_obs = dig_eteint
            report "Erreur valeur de sortie au reset" severity failure;
        assert dot1_obs = '0'
            report "Erreur valeur de sortie au reset" severity failure;
        assert dot2_obs = '0'
            report "Erreur valeur de sortie au reset" severity failure;
        assert dot3_obs = '0'
            report "Erreur valeur de sortie au reset" severity failure;
        assert dot4_obs = '0'
            report "Erreur valeur de sortie au reset" severity failure;
        rst_tb <= '0';
        wait;
    end process ; -- reset_process

    clock_stim : process
    begin
        clk_tb <= '0';
        wait for clk_period/2;
        clk_tb <= '1';
        wait for clk_period/2;

        if fin_testbench then
            wait;
        end if ;
    end process ; -- clock_stim


    test_process : process
    begin
        wait for 1 ms; -- wait for reset

        -- First test
        receive_data(address_remote_sti, key_pressed_sti, done_sti, valid_data_sti, x"01", key_9, '1');
        wait for 500 ms;
        receive_repeat(address_remote_sti, key_pressed_sti, done_sti, valid_data_sti, key_holded_sti, x"01", key_7, '1');
        wait for 500 ms; 
        
        fin_testbench <= true;
        wait;
    end process ; -- test_proc

end Behavioral;
