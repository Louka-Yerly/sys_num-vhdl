library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.pkg_constantes_functions.all;


entity NECDriver is
    Generic ( freq_horloge : positive := 50_000_000);
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           ir_signal : in STD_LOGIC;
           key_pressed, address_remote : out STD_LOGIC_VECTOR ((key_length/2)-1 downto 0);
           key_holded, valid_data, done : out STD_LOGIC);
end NECDriver;

architecture Behavioral of NECDriver is
    type ETATS is (standby, leading_pulse, space, address, key, wait_bit, detect_bit,
                    set_bit, wait_hold, holded_key, errorT, finishedT);
    type DUREE is (too_short, hold,  fit, too_long);
    
    -- Constants
    constant fin_pulse_ts_cst : positive := (freq_horloge*8)/1000;  -- 8 ms too_short
    constant fin_pulse_tl_cst : positive := (freq_horloge*1)/100; -- 10 ms too_long 

    constant fin_space_ts_cst : positive := (freq_horloge*1)/1000;  -- 1 ms too_short
    constant fin_space_hold_cst   : positive := (freq_horloge*4)/1000; -- 4 ms hold
    constant fin_space_tl_cst   : positive := (freq_horloge*5)/1000; -- 5 ms too_long

    constant fin_wait_cst : positive := (freq_horloge*1)/1000; -- 1 ms pour que la machine d'état ne reste pas bloquée
    
    constant timer27_cst   : positive := (freq_horloge*3)/100;  -- 30 ms pour que la machine d'état ne reste pas bloquée 
                                                                  -- (30 ms pour avoir un peu de marge sur les 27 ms)

    constant fin_repeat_hold_cst : positive := (freq_horloge*12)/100; -- 120 ms

    constant bit_detection_1_0 : positive := (freq_horloge*1)/1000; -- Seuil pour la détection d'un bit
    constant max_bit_duration  : positive := (freq_horloge*2)/1000; -- Durée maximale d'un bit (pris sur le bit '1' arrondi au dessus)
 
    -- State machine
    signal etat_present, etat_futur : ETATS;

    -- Gestion du temps
    signal fin_pulse  : DUREE;
    signal fin_space  : DUREE;
    signal fin_wait   : DUREE;
    signal timer27    : DUREE;
    signal fin_hold   : DUREE;
    signal sig_startC : STD_LOGIC; 
    signal sig_time   : STD_LOGIC_VECTOR(ntob(fin_repeat_hold_cst) downto 0); -- Par rapport au plus long temps + 1 bit pour doubler le temps

    -- Gestion de l'adresse 
    signal fin_address        : STD_LOGIC;
    signal sig_address_remote : STD_LOGIC_VECTOR(addr_length-1 downto 0);

    -- Gestion de la key
    signal fin_key         : STD_LOGIC;
    signal sig_key_holded  : STD_LOGIC;
    signal sig_key_pressed : STD_LOGIC_VECTOR(key_length-1 downto 0);

    -- Vecteur pour la reception des bits
    signal sig_data : STD_LOGIC_VECTOR(addr_length+key_length-1 downto 0);

    -- Gestion d'erreur
    signal sig_valid_data : STD_LOGIC;
    
    -- Gestion des bits
    signal current_bit : STD_LOGIC_VECTOR(ntob(addr_length+key_length) downto 0);
    signal compte_bit  : STD_LOGIC;
    signal incr_bit    : STD_LOGIC;
    signal sig_1_0     : STD_LOGIC; -- La valeur du bit actuel
    signal sig_detect_bit  : STD_LOGIC;
    signal incr_detect_bit : STD_LOGIC;
    signal sig_time_detect : STD_LOGIC_VECTOR(ntob(max_bit_duration) downto 0);
    


    
    
    -- Function checksum
    function checksum(signal bits : STD_LOGIC_VECTOR(key_length-1 downto 0)) return STD_LOGIC is
        constant var_OK    : STD_LOGIC_VECTOR(((bits'left+1)/2)-1 downto 0) := (others => '0');
        variable var_inter : STD_LOGIC_VECTOR(var_OK'range);
    begin
        var_inter := bits(bits'left downto (bits'length)/2) xnor bits(((bits'length)/2)-1 downto 0);
        if var_inter = var_OK then
            return '1';
        else
            return '0';
        end if ;
    end checksum;

begin
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    key_pressed <= sig_key_pressed((key_length/2)-1 downto 0); -- Donne la key de poids faible
    address_remote <= sig_address_remote((addr_length/2)-1 downto 0); -- Donne l'adresse de poids faible
    key_holded <= sig_key_holded;
    valid_data <= '1' when sig_valid_data = '1' and etat_present = finishedT else '0';
    done <= '1' when etat_present = finishedT or etat_present = errorT else '0';
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------   

    fin_pulse <= too_short when etat_present = leading_pulse and
                                unsigned(sig_time) < to_unsigned(fin_pulse_ts_cst, sig_time'length) else
                 too_long  when etat_present = leading_pulse and
                                unsigned(sig_time) > to_unsigned(fin_pulse_tl_cst, sig_time'length) else
                 fit;
    fin_space <= too_short when etat_present = space and
                                unsigned(sig_time) < to_unsigned(fin_space_ts_cst, sig_time'length) else
                 hold      when etat_present = space and
                                unsigned(sig_time) < to_unsigned(fin_space_hold_cst, sig_time'length) else
                 too_long  when etat_present = space and
                                unsigned(sig_time) > to_unsigned(fin_space_tl_cst, sig_time'length) else
                 fit;

    fin_wait <= too_long when etat_present = wait_hold and
                            unsigned(sig_time) > to_unsigned(fin_wait_cst, sig_time'length) else
                fit;
    timer27 <= too_long when unsigned(sig_time) > to_unsigned(timer27_cst, sig_time'length) else
               fit;

    sig_startC <= '1' when  (etat_present = standby and etat_futur = standby and sig_key_holded = '1') or
                            (etat_present = leading_pulse and etat_futur = leading_pulse) or -- Détecte les transition pour réinitialiser le compteur
                            (etat_present = space and etat_futur = space) or
                            (etat_present = wait_hold and etat_futur = wait_hold) or
                            etat_present = wait_bit or etat_present = detect_bit or etat_present = set_bit else
                  '0';

    fin_hold <= too_long when etat_present = standby and
                              unsigned(sig_time) > to_unsigned(fin_repeat_hold_cst, sig_time'length) else
                fit; 
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    fin_address <= '1' when unsigned(current_bit) = to_unsigned(addr_length-1, current_bit'length) else '0';
    sig_address_remote <= sig_data(addr_length+key_length-1 downto addr_length);

    fin_key <= '1' when unsigned(current_bit) = to_unsigned(addr_length+key_length-1, current_bit'length) else '0';
    sig_key_pressed <= sig_data(key_length-1 downto 0);

    compte_bit <= '1' when etat_present = key or etat_present = wait_bit or etat_present = detect_bit or
                            etat_present = set_bit
                      else '0';
    incr_bit <= '1' when etat_present = set_bit else '0';

    sig_valid_data <= '1' when checksum(sig_key_pressed) = '1' and checksum(sig_address_remote) = '1'
                          else '0';
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    sig_detect_bit <= '1' when etat_present = detect_bit or etat_present = set_bit else '0';
    incr_detect_bit <= '1' when etat_present = detect_bit else '0';
    sig_1_0 <= '1' when unsigned(sig_time_detect) > to_unsigned(bit_detection_1_0, sig_time_detect'length) else '0';
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    compteur_time : entity work.compteur_generic(Behavioral)
        port map(clk => clk, rst => rst, startC => sig_startC, incr => '1', value => sig_time);

    compteur_detection_bit : entity work.compteur_generic(Behavioral)
        port map(clk => clk, rst => rst, startC => sig_detect_bit, incr => incr_detect_bit, value => sig_time_detect);

    compteur_current_bit : entity work.compteur_generic(Behavioral)
        port map(clk => clk, rst => rst, startC => compte_bit, incr => incr_bit, value => current_bit);
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    registre : process(clk, rst)
    begin
        if rst = '1' then
            etat_present <= standby;
        elsif rising_edge(clk) then
            etat_present <= etat_futur;
        end if ;
    end process ; -- registre

    state_machine : process(etat_present, ir_signal, fin_pulse, fin_space, fin_wait, fin_address, fin_key, timer27, sig_valid_data)
    begin
        case etat_present is        
            when standby =>
                if ir_signal = '0' then
                    etat_futur <= leading_pulse;
                else
                    etat_futur <= standby;
                end if ;

            when leading_pulse =>
                if (fin_pulse = too_short and ir_signal = '1') or fin_pulse = too_long then
                    etat_futur <= standby;
                elsif ir_signal = '1' then
                    etat_futur <= space;
                else
                    etat_futur <= leading_pulse;
                end if ;

            when space =>
                if (fin_space = too_short and ir_signal = '0') or fin_space = too_long then
                    etat_futur <= errorT; 
                elsif fin_space = hold and ir_signal = '0' then
                    etat_futur <= wait_hold;
                elsif ir_signal = '0' then
                    etat_futur <= address;
                else
                    etat_futur <= space;
                end if ;
            
            when wait_hold =>
                if fin_wait = too_long then
                    etat_futur <= errorT;
                elsif ir_signal = '1' then
                    etat_futur <= holded_key;
                else
                    etat_futur <= wait_hold;
                end if ;

            when holded_key =>
                etat_futur <= standby;

            when address =>
                etat_futur <= wait_bit;

            when key =>
                etat_futur <= wait_bit;

            when wait_bit =>
                if timer27 = too_long then
                    etat_futur <= errorT;
                elsif ir_signal = '1' then
                    etat_futur <= detect_bit;
                else
                    etat_futur <= wait_bit;
                end if ;

            when detect_bit =>
                if timer27 = too_long then
                    etat_futur <= errorT;
                elsif ir_signal = '0' then
                    etat_futur <= set_bit;
                else
                    etat_futur <= detect_bit;
                end if ;

            when set_bit =>
                if fin_address = '1' then
                    etat_futur <= key;
                elsif fin_key = '1' and sig_valid_data = '1' then
                    etat_futur <= finishedT;
                elsif fin_key = '1' and sig_valid_data = '0' then
                    etat_futur <= errorT;
                else
                    etat_futur <= wait_bit;
                end if ;

            when errorT =>
                etat_futur <= standby;

            when finishedT =>
                etat_futur <= standby;

            when others =>
                etat_futur <= standby;
        end case ;      
    end process ; -- state_machine
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    registre_sig_key_holded : process(clk, rst)
    begin
        if rst = '1' then
            sig_key_holded <= '0';
        elsif rising_edge(clk) then
            if sig_key_holded = '1' then
                if etat_present = address or fin_hold = too_long then
                    sig_key_holded <= '0';
                end if ;
            else
                if etat_present = holded_key then
                    sig_key_holded <= '1';
                end if ;
            end if ;
        end if ;
        
    end process ; -- registre_sig_key_holded
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

    registre_sig_data : process(clk, rst)
    begin
        if rst = '1' then
            sig_data <= (others => '0');
        elsif rising_edge(clk) then
            if etat_present = set_bit then
                sig_data(to_integer((addr_length+key_length-1)-unsigned(current_bit))) <= sig_1_0;
            end if ;
        end if ;
    end process ; -- registre_sig_data
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------

end Behavioral;
