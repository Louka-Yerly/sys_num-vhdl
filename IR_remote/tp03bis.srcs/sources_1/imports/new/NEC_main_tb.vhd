library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.pkg_constantes_functions.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NEC_main_tb is
end NEC_main_tb;

architecture Behavioral of NEC_main_tb is
	constant address_remote_cst : STD_LOGIC_VECTOR((addr_length/2)-1 downto 0) := x"01";

    signal clk_tb : STD_LOGIC := '0';
    signal clk_period : time := 20 ns;
    signal rst_tb : STD_LOGIC := '0';

    signal fin_testbench : boolean := false;

    signal error_pulse : std_logic;
    constant error_pulse_duration : time := 1 ns;


    signal ir_signal_sti : STD_LOGIC := '1';
    signal key_pressed_obs : STD_LOGIC_VECTOR((key_length/2)-1 downto 0);
    signal address_remote_obs : STD_LOGIC_VECTOR((addr_length/2)-1 downto 0);
    signal key_holded_obs : STD_LOGIC;
    signal valid_data_obs : STD_LOGIC;
    signal done_obs : STD_LOGIC;
    signal dig1_obs, dig2_obs, dig3_obs, dig4_obs : STD_LOGIC_VECTOR (3 downto 0);
    signal dot1_obs, dot2_obs, dot3_obs, dot4_obs : STD_LOGIC;

    procedure start_transmission(signal ir : out STD_LOGIC) is
        constant leading_pulse : time := 9 ms;
        constant space : time := 4.5 ms;
    begin
        ir <= '0';
        wait for leading_pulse;
        ir <= '1';
        wait for space;
    end start_transmission;

    procedure finish_transmission(signal ir : out STD_LOGIC) is
        constant pulse_burst : time := 562.5 us;
    begin
        ir <= '0';
        wait for pulse_burst;
        ir <= '1';
    end finish_transmission;

    procedure send_key_address( signal ir : out STD_LOGIC;
                                constant key_or_addresse : in STD_LOGIC_VECTOR(key_pressed_obs'range)) is
        constant pulse_burst : time := 562.5 us;
        constant space_0 : time := 562.5 us;
        constant space_1 : time := 1.6875 ms;
    begin
        for i in key_length-1 downto 0 loop
            ir <= '0';
            wait for pulse_burst;
            ir <= '1';

            if i < key_length/2 then
                if key_or_addresse(i) = '1' then
                    wait for space_1;
                else
                    wait for space_0;
                end if ;
            else
                if key_or_addresse(i-8) = '1' then
                    wait for space_0;
                else
                    wait for space_1;
                end if ;
            end if ; 
        end loop ;
    end send_key_address;

    procedure repeat_code( signal ir : out STD_LOGIC) is
        constant leading_pulse : time := 9 ms;
        constant space : time := 2.25 ms;
        constant pulse_burst : time := 562.5 us;
    begin
        ir <= '0';
        wait for leading_pulse;
        ir <= '1';
        wait for space;
        ir <= '0';
        wait for pulse_burst;
        ir <= '1';
    end repeat_code;
    
    procedure transmit_all( signal ir : out STD_LOGIC;
                            constant addresse : in STD_LOGIC_VECTOR(address_remote_obs'range);
                            constant key : in STD_LOGIC_VECTOR(key_pressed_obs'range)) is
    begin
        start_transmission(ir);
        send_key_address(ir, addresse);
        send_key_address(ir, key);
        finish_transmission(ir);        
    end transmit_all;
begin
	UUT_NEC : entity work.NECDriver(Behavioral)
            port map(clk => clk_tb, rst => rst_tb, ir_signal => ir_signal_sti, key_pressed => key_pressed_obs,
                    address_remote => address_remote_obs, key_holded => key_holded_obs, valid_data => valid_data_obs,
                    done => done_obs);
    UUT_main : entity work.main(Behavioral)
            port map(clk => clk_tb, rst => rst_tb, key_pressed => key_pressed_obs,
                    address_remote => address_remote_obs, key_holded => key_holded_obs,
                    valid_data => valid_data_obs, done => done_obs,
                    dig1 => dig1_obs, dig2 => dig2_obs, dig3 => dig3_obs, dig4 => dig4_obs,
                    dot1 => dot1_obs, dot2 => dot2_obs, dot3 => dot3_obs, dot4 => dot4_obs);
    
    reset_process : process
    begin
        rst_tb <= '1';
        wait for clk_period;
        assert dig1_obs = dig_eteint
            report "Erreur valeur de sortie au reset" severity failure;
        assert dig2_obs = dig_eteint
            report "Erreur valeur de sortie au reset" severity failure;
        assert dig3_obs = dig_eteint
            report "Erreur valeur de sortie au reset" severity failure;
        assert dig4_obs = dig_eteint
            report "Erreur valeur de sortie au reset" severity failure;
        assert dot1_obs = '0'
            report "Erreur valeur de sortie au reset" severity failure;
        assert dot2_obs = '0'
            report "Erreur valeur de sortie au reset" severity failure;
        assert dot3_obs = '0'
            report "Erreur valeur de sortie au reset" severity failure;
        assert dot4_obs = '0'
            report "Erreur valeur de sortie au reset" severity failure;
        rst_tb <= '0';
        wait;
    end process ; -- reset_process

    clock_stim : process
    begin
        clk_tb <= '0';
        wait for clk_period/2;
        clk_tb <= '1';
        wait for clk_period/2;

        if fin_testbench then
            wait;
        end if ;
    end process ; -- clock_stim


    test_process : process
    begin
        wait for 1 ms; -- wait for reset

        -- First test
        transmit_all(ir_signal_sti, address_remote_cst, key_9);
        
        wait for 40.5 ms;
        repeat_code(ir_signal_sti);
        wait for 1 ms;

        wait for 96.1875 ms;
        repeat_code(ir_signal_sti);

        wait for 96.1875 ms;
        repeat_code(ir_signal_sti);

        wait for 96.1875 ms;
        repeat_code(ir_signal_sti);
        wait for 96.1875 ms;
        repeat_code(ir_signal_sti);
        wait for 96.1875 ms;
        repeat_code(ir_signal_sti);
        wait for 96.1875 ms;
        repeat_code(ir_signal_sti);
        wait for 96.1875 ms;
        repeat_code(ir_signal_sti);
        wait for 96.1875 ms;
        repeat_code(ir_signal_sti);




        wait for 200 ms;
        transmit_all(ir_signal_sti, address_remote_cst, key_7);
        
        wait for 220 ms;
        fin_testbench <= true;
        wait;
    end process ; -- test_proc


end Behavioral;
