library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity compteur_generic is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           incr: in STD_LOGIC;
           startC : in STD_LOGIC;
           value : out STD_LOGIC_VECTOR);
end compteur_generic;

architecture Behavioral of compteur_generic is
	signal counter_s : unsigned(value'range);

begin
	value <= STD_LOGIC_VECTOR(counter_s);

	registre : process(clk, rst)
	begin
		if rst = '1' then
			counter_s <= (others => '0');
		elsif rising_edge(clk) then
			if startC = '1' then
				if incr = '1' then
					counter_s <= counter_s+1;	
				end if ;
			else counter_s <= (others => '0');	
			end if ;
		end if ;
	end process ; -- registre
end Behavioral;
