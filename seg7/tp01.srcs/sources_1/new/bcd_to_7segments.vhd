----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/28/2020 09:58:16 AM
-- Design Name: 
-- Module Name: bcd_to_7segments - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bcd_to_7segments is
    Port ( bcd : in STD_LOGIC_VECTOR (4 downto 0); -- (MSB...LSB, Point)
           digit : out STD_LOGIC_VECTOR (7 downto 0)); -- (11-A, 7-B, 4-C, 2-D, 1-E, 10-F, 5-G, 3-POINTs)
end bcd_to_7segments;

                                            -- (11-A, 7-B, 4-C, 2-D, 1-E, 10-F, 5-G)
											--
											-- __A_
											--F|   |B
											-- |_G_|
											--E|   |
											-- |_D_|C


architecture Behavioral of bcd_to_7segments is

begin
    digit <= "1111110"&bcd(0) when bcd(4 downto 1) = "0000" else -- 0
		      "0110000"&bcd(0) when bcd(4 downto 1) = "0001" else -- 1
		      "1101101"&bcd(0) when bcd(4 downto 1) = "0010" else -- 2
		      "1111001"&bcd(0) when bcd(4 downto 1) = "0011" else -- 3
		      "0110011"&bcd(0) when bcd(4 downto 1) = "0100" else -- 4
		      "1011011"&bcd(0) when bcd(4 downto 1) = "0101" else -- 5
		      "1011111"&bcd(0) when bcd(4 downto 1) = "0110" else -- 6
		      "1110000"&bcd(0) when bcd(4 downto 1) = "0111" else -- 7
		      "1111111"&bcd(0) when bcd(4 downto 1) = "1000" else -- 8
		      "1111011"&bcd(0) when bcd(4 downto 1) = "1001" else -- 9
		      "0000000"&bcd(0);						 -- others 


end Behavioral;
