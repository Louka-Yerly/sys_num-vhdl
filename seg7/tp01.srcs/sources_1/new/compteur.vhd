----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/28/2020 10:10:35 AM
-- Design Name: 
-- Module Name: compteur - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity compteur is
    Port ( clk : in STD_LOGIC;
           nrst : in STD_LOGIC;
           en : in STD_LOGIC;
           fin_compteur_out : out STD_LOGIC);
end compteur;

architecture Behavioral of compteur is
    constant max : natural := 3; -- Choose between 2 and 3 3: less precise more comprehensible, 2 inverse
	signal counter_s : unsigned(1 downto 0);
	signal fin_compteur : std_logic;

begin
    fin_compteur_out <= fin_compteur;
    fin_compteur <= '1' when counter_s = to_unsigned(max, counter_s'length) and en = '1' else '0';

	compte:process(clk, nrst)
    begin
        if nrst = '0' then
            counter_s <= (others => '0');
        elsif rising_edge(clk) then
            if fin_compteur = '1' then
                counter_s <= (others => '0');
            else
                counter_s <= counter_s + 1;
            end if;
        end if;
    end process;


end Behavioral;
