----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/05/2020 11:48:36 PM
-- Design Name: 
-- Module Name: testDriver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity testDriver is
    Port ( clk : in STD_LOGIC;
           nrst : in STD_LOGIC;
           segment_in : out STD_LOGIC_VECTOR (7 downto 0);
           segment_out : out STD_LOGIC_VECTOR (3 downto 0));
end testDriver;

architecture Behavioral of testDriver is
    type ETAT is (first, wait1, second, wait2, third, wait3);
    signal etat_present, etat_futur : ETAT;
    signal sig_enable : STD_LOGIC;
    signal fin_compteur: STD_LOGIC;
    signal counter_s : unsigned(31 downto 0);
    constant MAX : natural := 100000000;
    signal sig_bcd_1, sig_bcd_2, sig_bcd_3, sig_bcd_4 : STD_LOGIC_VECTOR(4 downto 0);
begin
    sig_enable <= '1';
      
    driver : entity work.driver(Behavioral)
		port map(clk => clk, nrst => nrst, enable => sig_enable, bcd_1 => sig_bcd_1, bcd_2 => sig_bcd_2,
		 bcd_3 => sig_bcd_3, bcd_4 => sig_bcd_4, segment_in => segment_in, segment_out => segment_out);
		 
    registre:process(clk, nrst)
    begin
      if nrst = '0' then
          etat_present <= first;
      elsif rising_edge(clk) then
          if fin_compteur = '1' then
              case etat_present is
                    when first =>
                         etat_present <= second;
                    when second =>
                         etat_present <= third;
                    when third =>
                         etat_present <= first;
                    when others =>
                         etat_present <= first;
                end case ;
          end if;
      end if;
  end process;
    
  fin_compteur <= '1' when counter_s = to_unsigned(MAX, counter_s'length) else '0';
    
     compte:process(clk, nrst)
     begin
         if nrst = '0' then
             counter_s <= (others => '0');
         elsif rising_edge(clk) then
             if fin_compteur = '1' then
                 counter_s <= (others => '0');
             else
                 counter_s <= counter_s + 1;
             end if;
         end if;
     end process;
     
     combi:process(etat_present)
          begin
            case etat_present is
              when first =>
                  sig_bcd_1 <= "00010";
                  sig_bcd_2 <= "00100";
                  sig_bcd_3 <= "00110";
                  sig_bcd_4 <= "01000";
              when second =>
                  sig_bcd_1 <= "01010";
                  sig_bcd_2 <= "01100";
                  sig_bcd_3 <= "01110";
                  sig_bcd_4 <= "10000";
              when third =>
                  sig_bcd_1 <= "10011";
                  sig_bcd_2 <= "11001";
                  sig_bcd_3 <= "11001";
                  sig_bcd_4 <= "11001";
              when others =>
                  sig_bcd_1 <= "10011";
                  sig_bcd_2 <= "11001";
                  sig_bcd_3 <= "11001";
                  sig_bcd_4 <= "11001";
            end case;     
          end process;
              		 
end Behavioral;
