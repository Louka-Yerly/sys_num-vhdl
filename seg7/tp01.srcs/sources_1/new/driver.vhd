----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/28/2020 10:09:11 AM
-- Design Name: 
-- Module Name: driver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity driver is
    Port ( clk : in STD_LOGIC;
           nrst : in STD_LOGIC;
           enable : in STD_LOGIC;
           bcd_1 : in STD_LOGIC_VECTOR (4 downto 0);
           bcd_2 : in STD_LOGIC_VECTOR (4 downto 0);
           bcd_3 : in STD_LOGIC_VECTOR (4 downto 0);
           bcd_4 : in STD_LOGIC_VECTOR (4 downto 0);
           segment_in : out STD_LOGIC_VECTOR (7 downto 0);
           segment_out : out STD_LOGIC_VECTOR (3 downto 0));
end driver;

architecture Behavioral of driver is
    type ETATS is (first, second, third, fourth);
	signal etat_present, etat_futur : ETATS;
	signal sig_fin_compteur : std_logic;
	signal digit_1, digit_2, digit_3, digit_4 : STD_LOGIC_VECTOR(7 downto 0);
begin
    
    converter_1 : entity work.bcd_to_7segments(Behavioral)
		port map(bcd => bcd_1, digit => digit_1);
	converter_2 : entity work.bcd_to_7segments(Behavioral)
		port map(bcd => bcd_2, digit => digit_2);
	converter_3 : entity work.bcd_to_7segments(Behavioral)
		port map(bcd => bcd_3, digit => digit_3);
	converter_4 : entity work.bcd_to_7segments(Behavioral)
		port map(bcd => bcd_4, digit => digit_4);

	counter : entity work.compteur(Behavioral)
		port map(clk => clk, nrst => nrst, en => enable, fin_compteur_out => sig_fin_compteur);


	registre : process(clk, nrst)
	begin
		if nrst = '0' then
			etat_present <= first;
		elsif rising_edge(clk) then
			etat_present <= etat_futur;
		end if ;
	end process ; -- registre

	state_machine : process(etat_present, sig_fin_compteur)
	begin
		if sig_fin_compteur = '1' then
			case etat_present is
				when first =>
					etat_futur <= second;
				when second =>
					etat_futur <= third;
				when third =>
					etat_futur <= fourth;
				when fourth =>
					etat_futur <= first;
				when others =>
				    etat_futur <= first;
			end case ;
		else
			etat_futur <= etat_present;
		end if ;		
	end process ; -- state_machine



	combi : process(etat_present, digit_1, digit_2, digit_3, digit_4)
		constant out_1 : std_logic_vector(3 downto 0) := "0111"; -- (12, 9, 8, 6)
		constant out_2 : std_logic_vector(3 downto 0) := "1011";
		constant out_3 : std_logic_vector(3 downto 0) := "1101";
		constant out_4 : std_logic_vector(3 downto 0) := "1110";
	begin
		case etat_present is		 
		 	when first =>
		 		segment_in <= digit_1;
		 		segment_out <= out_1;
		 	when second =>
		 		segment_in <= digit_2;
		 		segment_out <= out_2; 
		 	when third =>
		 		segment_in <= digit_3;
		 		segment_out <= out_3;
		 	when fourth =>
		 		segment_in <= digit_4;
		 		segment_out <= out_4;
		 	when others =>
		 		segment_in <= (others => '0');
		 		segment_out <= (others => '0');
		 end case ;
	end process ; -- combi
end Behavioral;
