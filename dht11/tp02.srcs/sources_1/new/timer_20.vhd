----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/28/2020 05:40:52 AM
-- Design Name: 
-- Module Name: timer_20 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity timer_20 is
    Generic(freq_horloge : integer := 50_000_000);
    Port (
        rst : in STD_LOGIC;
        clk : in STD_LOGIC;
        startTimer :in STD_LOGIC;
        endTimer :out STD_LOGIC);
end timer_20;

architecture Behavioral of timer_20 is
    constant max : natural := (freq_horloge*2)/100;
	signal counter_s : unsigned(19 downto 0);
	signal endTimer_s : std_logic;
    
    begin
        endTimer <= endTimer_s;
        endTimer_s <= '1' when counter_s = to_unsigned(max, counter_s'length) else '0';
    
        compte:process(clk, rst)
        begin
            if rst = '1' then
                counter_s <= (others => '0');
            elsif rising_edge(clk) then
                if startTimer = '1' then
                    counter_s <= counter_s + 1;
                else 
                    counter_s <= (others => '0'); 
                end if;
            end if;
        end process;
    
    
end Behavioral;
    