----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/28/2020 05:40:52 AM
-- Design Name: 
-- Module Name: dht11Driver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dht11Driver is
	generic(freq_horloge : integer := 50_000_000);
	Port ( sensorSignal_i : in STD_LOGIC;
           sensorSignal_o : out STD_LOGIC;
           sensorSignal_t : out std_logic;
           rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           start : in STD_LOGIC;
           done : out STD_LOGIC;
           data : out STD_LOGIC_VECTOR(39 downto 0);
           validData : out STD_LOGIC);
end dht11Driver;

architecture Behavioral of dht11Driver is
	-- type
	type ETATS is (standby, start0, waitDht, dht0, dht1, waitBit, detectBit, setBit, checkData, errorT, finished);
	
	-- constant
	constant bit_min : integer  := (data'right)-1; -- limit to reset the current_bit
	constant limite_1 : integer := 40; -- number of microsecondes to determine 0/1 in transmission
	constant sig_sensorSignal_o : STD_LOGIC := '0';
	
	-- signal
	signal etat_present, etat_futur : ETATS;
	signal current_bit : signed(6 downto 0); -- index of the current bit	
	signal finTransmission : STD_LOGIC;
	signal sig_1_0 : STD_LOGIC;

	-- timeout
	signal sig_startTimeout, sig_endTimeout : STD_LOGIC;
	-- timer
	signal sig_startTimer, sig_endTimer : STD_LOGIC;
	-- compteur
	signal sig_startCompteur : STD_LOGIC;
	signal sig_value : STD_LOGIC_VECTOR(6 downto 0); -- (0 to 127)
	-- checksum
	signal sig_data : STD_LOGIC_VECTOR(data'range);
	signal sig_validData : STD_LOGIC;


begin
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

	sig_startTimer <= '1' when etat_present = start0 and sig_endTimer = '0' else '0';
	sig_startTimeout <= '0' when 	etat_present = standby or
									etat_present = start0 or
									etat_present = checkData or
									etat_present = errorT or
									etat_present = finished
									else '1'; 
	sig_startCompteur <= '1' when etat_present = detectBit else '0';
	finTransmission <= '1' when current_bit = to_signed(bit_min, current_bit'length) else '0'; --Quand le bit de transmission = -1
	sig_1_0 <= '1' when unsigned(sig_value) > to_unsigned(limite_1, sig_value'length) else '0';

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

	done <= '1' when etat_present = finished or etat_present = errorT else '0';
	sensorSignal_o <= sig_sensorSignal_o;
	sensorSignal_t <= '0' when etat_present = start0 else '1';
	validData <= '1' when etat_present = finished else '0';
	data <= sig_data;
	 
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

	timer1 : entity work.timer_20(Behavioral)
		generic map(freq_horloge => freq_horloge)
		port map(clk => clk, rst => rst, startTimer => sig_startTimer, endTimer => sig_endTimer);
	timeout1 : entity work.timeout(Behavioral)
		generic map(freq_horloge => freq_horloge)
		port map(clk => clk, rst => rst, startTimeout => sig_startTimeout, endTimeout => sig_endTimeout);
	compteur1 : entity work.compteur_us(Behavioral)
		generic map(freq_horloge => freq_horloge)
		port map(clk => clk, rst => rst, startCompteur => sig_startCompteur, value => sig_value);
	checkSum1 : entity work.checkSum(Behavioral)
		port map(data => sig_data, validData => sig_validData);

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

	registre_etat : process(clk, rst)
	begin
		if rst = '1' then
			etat_present <= standby;
		elsif rising_edge(clk) then
			etat_present <= etat_futur;
		end if ;
	end process ; -- registre

	state_machine : process(etat_present, start, sig_endTimer, sensorSignal_i, finTransmission, sig_endTimeout, sig_validData)
	begin
		case etat_present is
		
			when standby =>
				if start = '1' then
					etat_futur <= start0;
				else
					etat_futur <= standby;
				end if ;
			
			when start0 =>
				if sig_endTimer = '1' then
					etat_futur <= waitDht;
				else
					etat_futur <= start0;
				end if ;

			when waitDht =>
				if sig_endTimeout = '1' then
					etat_futur <= errorT;
				elsif sensorSignal_i = '0' then
					etat_futur <= dht0;
				else
					etat_futur <= waitDht;
				end if ;

			when dht0 =>
				if sig_endTimeout = '1' then
					etat_futur <= errorT;
				elsif sensorSignal_i = '1' then
					etat_futur <= dht1;
				else
					etat_futur <= dht0;
				end if ;

			when dht1 =>
				if sig_endTimeout = '1' then
					etat_futur <= errorT;
				elsif sensorSignal_i = '0' then
					etat_futur <= waitBit;
				else
					etat_futur <= dht1;
				end if ;

			when waitBit =>
				if sig_endTimeout = '1' then
					etat_futur <= errorT;
				elsif finTransmission = '1' then
					etat_futur <= checkData;
				elsif sensorSignal_i = '1' then
					etat_futur <= detectBit;
				else
					etat_futur <= waitBit;
				end if ;

			when detectBit =>
				if sig_endTimeout = '1' then
					etat_futur <= errorT;
				elsif sensorSignal_i = '0' then
					etat_futur <= setBit;
				else
					etat_futur <= detectBit;
				end if ;

			when setBit =>
				etat_futur <= waitBit;

			when checkData =>
				if sig_validData = '1' then
					etat_futur <= finished;
				else
					etat_futur <= errorT;
				end if ;

			when errorT =>
				etat_futur <= standby;

			when finished =>
				etat_futur <= standby;

			when others =>
				etat_futur <= standby;
		end case ;
	end process ; -- combi

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

	registre_current_bit: process(clk, rst)
	begin
		if rst = '1' then
			current_bit <= to_signed(data'left, current_bit'length);
		elsif rising_edge(clk) then
			if etat_present = setBit then
				current_bit <= current_bit-1;
			elsif finTransmission = '1' then
				current_bit <= to_signed(data'left, current_bit'length);
			end if ;
		end if ;
	end process ; -- combi_current_bit

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

	registre_data : process(clk, rst)
	begin
		if rst = '1' then
			sig_data <= (others => '0');
		elsif rising_edge(clk) then
			if etat_present = setBit then
				sig_data(to_integer(current_bit)) <= sig_1_0;
			end if ;			
		end if ;
	end process ; -- registre_data

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

end Behavioral;