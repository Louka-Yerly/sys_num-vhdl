----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/10/2018 07:40:11 PM
-- Design Name: 
-- Module Name: filtre - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity filtre is
  generic(freq_horloge : integer := 50_000_000);
  Port (   sig_in : in STD_LOGIC;
           sig_out : out STD_LOGIC;
           clk : in STD_LOGIC;
           rst : in STD_LOGIC);
end filtre;

architecture Behavioral of filtre is
  constant taille_filtre : integer := freq_horloge/50000;
  signal compteur        : unsigned(10 downto 0);
  signal sig_courant     : std_logic;

begin
  sig_out <= sig_courant;

  process(rst, clk)
  begin
    if rst = '1' then
      sig_courant <= '1';
      compteur <= (others => '0');
    elsif rising_edge(clk) then
      if sig_courant /= sig_in then
        if compteur >= to_unsigned(taille_filtre, compteur'length) then
          compteur <= (others => '0');
          sig_courant <= not sig_courant;
        else
          compteur <= compteur + 1;
        end if;
      else
        compteur <= (others => '0');
      end if;
    end if;
  end process;


end Behavioral;
