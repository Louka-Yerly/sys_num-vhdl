library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package pkg_constantes is

    constant dig_c      : std_logic_vector(3 downto 0) := "1010";
    constant dig_t      : std_logic_vector(3 downto 0) := "1011";
    constant dig_h      : std_logic_vector(3 downto 0) := "1100";
    constant dig_e      : std_logic_vector(3 downto 0) := "1101";
    constant dig_eteint : std_logic_vector(3 downto 0) := "1111";

end pkg_constantes;