library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity asyncInputSynchronization is
    generic(SYNC_STAGES : integer := 3;
            PIPELINE_STAGES : integer := 1);
    Port ( clk : in STD_LOGIC;
           async_in : in STD_LOGIC; -- Async Input <async_in>
           sync_out : out STD_LOGIC -- Synchronized output
    );
end asyncInputSynchronization;

architecture Behavioral of asyncInputSynchronization is

    -- Asynchronous Input Synchronization
    --
    -- The following code is an example of synchronizing an asynchronous input
    -- of a design to reduce the probability of metastability affecting a circuit.
    --
    -- The following synthesis and implementation attributes are added to the code
    -- in order improve the MTBF characteristics of the implementation:
    --
    --  ASYNC_REG="TRUE" - Specifies registers will be receiving asynchronous data
    --                     input to allow tools to report and improve metastability
    --
    -- The following constants are available for customization:
    --
    --   SYNC_STAGES     - Integer value for number of synchronizing registers, must be 2 or higher
    --   PIPELINE_STAGES - Integer value for number of registers on the output of the
    --                     synchronizer for the purpose of improveing performance.
    --                     Particularly useful for high-fanout nets.
    --   INIT            - Initial value of synchronizer registers upon startup, 1'b0 or 1'b1.
    
    -- Insert the following before begin keyword of architecture
    
    constant INIT : std_logic := '0';
    
    signal sreg : std_logic_vector(SYNC_STAGES-1 downto 0) := (others => INIT);
    attribute async_reg : string;
    attribute async_reg of sreg : signal is "true";
    
    signal sreg_pipe : std_logic_vector(PIPELINE_STAGES-1 downto 0) := (others => INIT);
    attribute shreg_extract : string;
    attribute shreg_extract of sreg_pipe : signal is "false";

begin
   process(clk)
   begin
    if(clk'event and clk='1')then
       sreg <= sreg(SYNC_STAGES-2 downto 0) & async_in;  -- Async Input <async_in>
    end if;
   end process;

   no_pipeline : if PIPELINE_STAGES = 0 generate
   begin
      sync_out <= sreg(SYNC_STAGES-1);
   end generate;

   one_pipeline : if PIPELINE_STAGES = 1 generate
   begin
    process(clk)
    begin
      if(clk'event and clk='1') then
        sync_out <= sreg(SYNC_STAGES-1);
      end if;
    end process;
   end generate;

   multiple_pipeline : if PIPELINE_STAGES > 1 generate
   begin
    process(clk)
    begin
      if(clk'event and clk='1') then
        sreg_pipe <= sreg_pipe(PIPELINE_STAGES-2 downto 0) & sreg(SYNC_STAGES-1);
      end if;
    end process;
    sync_out <= sreg_pipe(PIPELINE_STAGES-1);
   end generate;


end Behavioral;