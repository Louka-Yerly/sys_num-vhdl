----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/28/2020 05:40:52 AM
-- Design Name: 
-- Module Name: checkSum - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity checkSum is
	Port (	data : in STD_LOGIC_VECTOR(39 downto 0);
			validData : out STD_LOGIC);
end checkSum;

architecture Behavioral of checkSum is
	signal valid : STD_LOGIC;
begin
	
	validData <= valid;
	
	combi : process(data)
		variable addition : unsigned(9 downto 0); -- 4*255 = 1020 => 10 bits 
	begin
		addition := (others => '0');

		addition := to_unsigned(	to_integer(unsigned(data(39 downto 32))) +
									to_integer(unsigned(data(31 downto 24))) +
									to_integer(unsigned(data(23 downto 16))) +
									to_integer(unsigned(data(15 downto 8))),
									addition'length);

		if addition(7 downto 0) = unsigned(data(7 downto 0)) then
			valid <= '1';
		else
			valid <= '0';
		end if ;
	end process ; -- combi
end Behavioral;
