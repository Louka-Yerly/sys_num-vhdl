----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/28/2020 05:40:52 AM
-- Design Name: 
-- Module Name: compteur_us - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity compteur_us is
	Generic(freq_horloge : integer := 50_000_000);
	Port (	clk : in STD_LOGIC;
			rst : in STD_LOGIC;
			startCompteur : in STD_LOGIC;
			value : out STD_LOGIC_VECTOR(6 downto 0));
end compteur_us;

architecture Behavioral of compteur_us is
	constant microseconde : natural := freq_horloge/1_000_000;

	signal counter_s : unsigned(5 downto 0);
	signal fin_compteur : STD_LOGIC;
	signal sig_value : unsigned(value'range);
begin
	
	fin_compteur <= '1' when counter_s = to_unsigned(microseconde, counter_s'length) else '0';
	value <= STD_LOGIC_VECTOR(sig_value);

	registre : process(clk, rst)
	begin
		if rst = '1' then
			counter_s <= (others => '0');
		elsif rising_edge(clk) then
			if startCompteur = '1' then
				if fin_compteur = '1' then
					counter_s <= (others => '0');
					sig_value <= sig_value+1;
				else
					counter_s <= counter_s+1;
				end if ;	
			else
				counter_s <= (others => '0');
				sig_value <= (others => '0');
			end if ;
			
		end if ;
	end process ; -- registre
		
end Behavioral;
