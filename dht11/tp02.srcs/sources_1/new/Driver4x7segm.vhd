library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.pkg_constantes.all;

entity Driver4x7segm is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           dig1, dig2, dig3, dig4 : in STD_LOGIC_VECTOR (3 downto 0);
           dot1, dot2, dot3, dot4 : in STD_LOGIC;
           pin11, pin7, pin4, pin2, pin1, pin10, pin5, pin3 : out std_logic;
           pin12, pin9, pin8, pin6: out std_logic);
end Driver4x7segm;

architecture Behavioral of Driver4x7segm is
    type state is (dig1State, dig2State, dig3State, dig4State);
    signal currentState, futurState : state;
    signal selectedBcd : std_logic_vector(3 downto 0);
    signal selectedDot : std_logic;
    signal outPins : std_logic_vector(6 downto 0);
    
    signal compteur : unsigned(3 downto 0);
    signal fin_compteur : std_logic;
begin

  fin_compteur <= '1' when compteur = "0011" else '0';
  
  attente:process(rst, clk)
  begin
      if rst = '1' then
          compteur <= (others => '0');
      elsif rising_edge(clk) then
          if fin_compteur = '0' then
              compteur <= compteur + 1;
          else
              compteur <= (others => '0');
          end if;
      end if;
  end process;

  calcul_etat_futur: process(currentState, fin_compteur)
  begin
    futurState <= currentState;
    case currentState is
        when dig1State => if fin_compteur = '1' then
                            futurState <= dig2State;
                          end if;
        when dig2State => if fin_compteur = '1' then
                            futurState <= dig3State;
                          end if;
        when dig3State => if fin_compteur = '1' then
                            futurState <= dig4State;
                          end if;
        when dig4State => if fin_compteur = '1' then
                            futurState <= dig1State;
                          end if;
        when others => futurState <= dig1State;
    end case;
  end process;
  
  registre:process(clk, rst)
  begin
    if rst = '1' then
        currentState <= dig1State;
    elsif rising_edge(clk) then
        currentState <= futurState;
    end if;
  end process;
  
  selectedBcd <= dig1 when currentState = dig1State else
                 dig2 when currentState = dig2State else
                 dig3 when currentState = dig3State else
                 dig4;
                 
  selectedDot <= dot1 when currentState = dig1State else
                 dot2 when currentState = dig2State else
                 dot3 when currentState = dig3State else
                 dot4;
                 
  pin3 <= selectedDot;
  
  decode: process(selectedBcd)
  begin
    case selectedBcd is
      when "0000" => outPins <= "1111110";
      when "0001" => outPins <= "0110000";
      when "0010" => outPins <= "1101101";
      when "0011" => outPins <= "1111001";
      when "0100" => outPins <= "0110011";
      when "0101" => outPins <= "1011011";
      when "0110" => outPins <= "1011111";
      when "0111" => outPins <= "1110000";
      when "1000" => outPins <= "1111111";
      when "1001" => outPins <= "1111011";
      when dig_c  => outPins <= "0001101";
      when dig_t  => outPins <= "0001111";
      when dig_h  => outPins <= "0110111";
      when dig_e  => outPins <= "1001111";
      when others => outPins <= (others => '0');
    end case;
  end process;

  pin12 <= '0' when currentState = dig1State else '1';
  pin9  <= '0' when currentState = dig2State else '1';
  pin8  <= '0' when currentState = dig3State else '1';
  pin6  <= '0' when currentState = dig4State else '1';
  
  (pin11, pin7, pin4, pin2, pin1, pin10, pin5) <= outPins;
  
end Behavioral;
