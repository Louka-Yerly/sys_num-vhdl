----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Nicolas Schroeter
-- 
-- Create Date: 09/06/2018 01:03:46 PM
-- Design Name: 
-- Module Name: main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.pkg_constantes.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
  generic(freq_horloge : integer := 50_000_000);
  port(clk      : in STD_LOGIC;
       rst     : in STD_LOGIC;
       -- signaux pour le driver dh11
       start    : out STD_LOGIC;
       done     : in STD_LOGIC;
       data     : in STD_LOGIC_VECTOR(39 downto 0);
       validData: in STD_LOGIC;
       -- signaux pour le driver 4x7 segments
       dig1, dig2, dig3, dig4 : out STD_LOGIC_VECTOR(3 downto 0);
       dot1, dot2, dot3, dot4 : out STD_LOGIC
       );
end main;

architecture Behavioral of main is
    
    type etats is (collecte, affiche_collecte, affiche_temperature, affiche_humidite, affiche_error);
    signal etat_present, etat_futur : etats;
    
    signal compteurTemps                  : unsigned(31 downto 0);
    signal finTemps                       : std_logic;
    constant dureeAffichage               : integer := 2 * freq_horloge; -- 2sec.
    
    signal memoireData                    : STD_LOGIC_VECTOR(data'range);
        
    signal compteurErreur                 : unsigned(7 downto 0);
    signal valeurBCD                      : std_logic_vector(11 downto 0);
    signal muxEntreeBinBCD                : std_logic_vector(7 downto 0);
    signal dig234                         : std_logic_vector(11 downto 0);
    
    signal rst_s : STD_LOGIC;
    
begin
        
    start <= '1' when etat_present = collecte else '0';
    
    decodeurDig1: process(etat_present)
    begin
        case etat_present is
            when collecte | affiche_collecte  => dig1 <= dig_c;
            when affiche_temperature =>          dig1 <= dig_t;
            when affiche_humidite =>             dig1 <= dig_h;
            when affiche_error =>                dig1 <= dig_e;
            when others =>                       dig1 <= dig_eteint;
        end case;
    end process;
    
    dig234 <=  valeurBCD(7 downto 0) & dig_eteint when (etat_present = affiche_temperature) or 
                                                       (etat_present = affiche_humidite) or 
                                                       (etat_present = affiche_error) else
               dig_eteint & dig_eteint & dig_eteint;
               
    dig2 <= dig234(11 downto 8); 
    dig3 <= dig234(7 downto 4);
    dig4 <= dig234(3 downto 0);
    dot1 <= '0';
    dot2 <= '0';
    dot3 <= '1';
    dot4 <= '0';

    muxEntreeBinBCD <=  memoireData(23 downto 16) when etat_present = affiche_temperature else
                        memoireData(39 downto 32) when etat_present = affiche_humidite else
                        std_logic_vector(compteurErreur) when etat_present = affiche_error else
                        (others => '0'); -- etats collecte ou parasites
                        
    decodeBCD: entity work.bin8bcd(struct)
        port map ( bin => muxEntreeBinBCD,
                   bcd => valeurBCD);
    
    finTemps <= '1' when compteurTemps >= to_unsigned(dureeAffichage, compteurTemps'length) else '0';
    temps:process(rst_s, clk)
    begin
        if rst = '1' then
            compteurTemps <= (others => '0');
        elsif rising_edge(clk) then
            if finTemps = '0' then
                compteurTemps <= compteurTemps + 1;
            else
                compteurTemps <= (others => '0');
            end if;
        end if;
    end process;
        
    combi_me: process(etat_present, finTemps, done)
    begin
        case etat_present is
            when collecte =>
                etat_futur <= affiche_collecte;
                
            when affiche_collecte =>
                if done = '1' then
                    etat_futur <= affiche_temperature;
                else
                    etat_futur <= affiche_collecte;
                end if;
                
            when affiche_temperature =>
                if finTemps = '1' then
                     etat_futur <= affiche_humidite;
                else
                    etat_futur <= affiche_temperature;
                end if;
 
             when affiche_humidite =>
                if finTemps = '1' then
                    etat_futur <= affiche_error;
                else
                    etat_futur <= affiche_humidite;
                end if;
                
             when affiche_error =>
                if finTemps = '1' then
                    etat_futur <= collecte;
                else
                    etat_futur <= affiche_error;
                end if;
                   
             when others =>
                etat_futur <= collecte;
                
         end case;
    end process combi_me;

    reg_me:process(rst, clk)
    begin
        if rst_s = '1' then
            etat_present <= affiche_error;
        elsif rising_edge(clk) then
            etat_present <= etat_futur;
        end if;
    end process;

    mem_data:process(rst, clk)
    begin
        if rst = '1' then
            memoireData    <= (others => '0');
        elsif rising_edge(clk) then
            if done = '1' and validData = '1' then
                memoireData <= data;
            end if;
        end if;
    end process;


    compteur_erreurs:process(rst, clk)
    begin
        if rst = '1' then
            compteurErreur <= (others => '0');
        elsif rising_edge(clk) then
            if done = '1' and validData = '0' then
                compteurErreur <= compteurErreur + 1;
            end if;
        end if;
    end process;
        
end Behavioral;
