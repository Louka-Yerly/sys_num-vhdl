-- reference : p.322 ug953-vivado-7series-libraries.pdf
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity tristate is
  port (
  O  : out std_logic;      -- Output (from buffer)
  IO : inout std_logic;    -- Port pin
  I  : in  std_logic;      -- Input (to buffer)
  T  : in  std_logic);     -- Tristate control
end tristate;

architecture Behavioral of tristate is

begin

IOBUF_inst: IOBUF
    port map( 
        O =>  O,
        IO => IO,
        I =>  I,
        T =>  T
    );

end Behavioral;
