----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/28/2020 06:49:16 AM
-- Design Name: 
-- Module Name: timeout - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity timeout is
	Generic(freq_horloge : integer := 50_000_000);
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           startTimeout : in STD_LOGIC;
           endTimeout : out STD_LOGIC);
end timeout;

architecture Behavioral of timeout is
	constant max : natural := (freq_horloge*7)/1_000; -- 100us + 100us + 40(50+70)us = 5000us = 5ms => marge => 7ms (40%)

	signal fin_compteur : STD_LOGIC;
	signal counter_s : unsigned(18 downto 0);
begin
	fin_compteur <= '1' when counter_s = to_unsigned(max, counter_s'length) else '0';
	endTimeout <= fin_compteur;


	registre : process(clk, rst)
	begin
		if rst = '1' then
			counter_s <= (others => '0');
		elsif rising_edge(clk) then
			if startTimeout = '1' then
				counter_s <= counter_s+1;
			else
				counter_s <= (others => '0');
			end if ;
		end if ;
	end process ; -- registre




end Behavioral;
